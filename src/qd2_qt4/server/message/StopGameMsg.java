package qd2_qt4.server.message;

import java.io.Serializable;

/**
 * A game message, to stop the game and show the results.
 * @author qt4
 *
 */
public class StopGameMsg implements Serializable{

	/**
	 * Serial number
	 */
	private static final long serialVersionUID = -4472206380097358097L;
	/**
	 * Results message
	 */
	private String winMsg;
	
	/**
	 * Constructor of the message.
	 * @param winMsg Results message.
	 */
	public StopGameMsg(String winMsg){
		this.winMsg = winMsg;
	}
	
	/**
	 * Get the result message.
	 * @return The result message that shows the winner.
	 */
	public String getMsg(){
		return winMsg;
	}
}