package qd2_qt4.server.message;
import provided.datapacket.ADataPacket;
import provided.datapacket.ADataPacketAlgoCmd;
import common.IAddCmd;
/**
 * Known message, add command message.
 * @author qt4
 *
 */
public class AddCmd implements IAddCmd{

	/**
	 * Serial number
	 */
	private static final long serialVersionUID = -3864101128974173234L;
	/**
	 * Class type as ID
	 */
	private Class<?> classtype;
	/**
	 * Command to run
	 */
	private ADataPacketAlgoCmd<ADataPacket, ?, Void> cmd;
	
	/**
	 * Constructor
	 * @param classtype Class type.
	 * @param cmd Command to run.
	 */
	public AddCmd(Class<?> classtype, ADataPacketAlgoCmd<ADataPacket, ?, Void> cmd){
		this.classtype = classtype;
		this.cmd = cmd;
	}
	
	/**
	 * Get command ID
	 * @return Class type as command ID.
	 */
	@Override
	public Class<?> getID() {
		return classtype;
	}
	/**
	 * Get command.
	 * @return command.
	 */
	@Override
	public ADataPacketAlgoCmd<ADataPacket, ?, Void> getCmd() {
		return cmd;
	}

}
