package qd2_qt4.server.message;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;
import java.util.UUID;

import common.IChatroom;

/**
 * A game message, to start the game.
 * @author qt4
 *
 */
public class StartGameMsg implements Serializable{

	/**
	 * Serial number
	 */
	private static final long serialVersionUID = -603535862326663741L;
	
	/**
	 * Initial alarms.
	 */
	private Map<UUID, ArrayList<Double>> alarms = new Hashtable<UUID, ArrayList<Double>>();
	
	/**
	 * A team includes all players.
	 */
	private IChatroom allTeam;
	
	/**
	 * Team index.
	 */
	private int teamIdx;
	
	/**
	 * Constructor of the message.
	 * @param alarms Init alarms.
	 * @param allTeam A team includes all players.
	 * @param teamIdx Team index.
	 */
	public StartGameMsg(Map<UUID, ArrayList<Double>> alarms, IChatroom allTeam, int teamIdx){
		this.alarms = alarms;
		this.allTeam = allTeam;
		this.teamIdx = teamIdx;
	}
	
	/**
	 * Get the index of the team.
	 * @return Index of a team.
	 */
	public int getTeamIdx(){
		return teamIdx;
	}
	
	/**
	 * Get the init alarms.
	 * @return Init alarm list.
	 */
	public Map<UUID, ArrayList<Double>> getInitAlarms(){
		return alarms;
	}
	
	/**
	 * Get the all team.
	 * @return An all team.
	 */
	public IChatroom getAllTeam(){
		return allTeam;
	}

}
