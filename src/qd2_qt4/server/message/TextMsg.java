package qd2_qt4.server.message;

import common.ITextMsg;

/**
 * Implementation of ITextMsg
 * @author qt4
 *
 */
public class TextMsg implements ITextMsg{

	/**
	 * Serial number
	 */
	private static final long serialVersionUID = -1111239797305262255L;
	/**
	 * Message text
	 */
	private String text;
	
	/**
	 * Constructor
	 * @param text Message text
	 */
	public TextMsg(String text){
		this.text=text;
	}
	/**
	 * Get the text content.
	 * @return The text content.
	 */
	@Override
	public String getText() {
		return text;
	}

}
