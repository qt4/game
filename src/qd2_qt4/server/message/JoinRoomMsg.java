package qd2_qt4.server.message;

import common.IJoinRoomMsg;
import common.IMember;

/**
 * A known message, join room message.
 * @author qt4
 *
 */
public class JoinRoomMsg implements IJoinRoomMsg{
	/**
	 * Serial number
	 */
	private static final long serialVersionUID = -4829993912818206261L;
	/**
	 * Member stub who joins the room.
	 */
	private IMember member;
	
	/**
	 * Constructor of the message.
	 * @param member Member stub who joins the room.
	 */
	public JoinRoomMsg(IMember member){
		this.member = member;
	}
	
	/**
	 * Get the member stub who joins the room
	 * @return Member stub who joins the room.
	 */
	@Override
	public IMember getMember() {
		return member;
	}

}
