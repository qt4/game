package qd2_qt4.server.message;

import java.io.Serializable;
import java.util.UUID;

/**
 * A game message, send collected alarm.
 * @author qt4
 *
 */
public class CollectMsg implements Serializable{

	/**
	 * Serial number
	 */
	private static final long serialVersionUID = 1092864123954824942L;
	
	/**
	 * Alamr UUID
	 */
	private UUID icon;
	
	/**
	 * Team UUID who collects the alarm.
	 */
	private UUID teamUUID;
	
	/**
	 * Get the team UUID.
	 * @return Team UUID.
	 */
	public UUID getTeamUUID(){
		return teamUUID;
	}
	
	/**
	 * Constructor of the message.
	 * @param icon Alarm UUID.
	 * @param teamUUID Team UUID.
	 */
	public CollectMsg(UUID icon, UUID teamUUID){
		this.icon = icon;
		this.teamUUID = teamUUID;
	}
	
	/**
	 * Get Alarm UUID.
	 * @return Alarm UUID.
	 */
	public UUID getIcon(){
		return icon;
	}
}
