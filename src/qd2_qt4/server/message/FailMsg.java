package qd2_qt4.server.message;

import common.IFailMsg;

/**
 * A known message, fail message
 * @author qt4
 *
 */
public class FailMsg implements IFailMsg{
	/**
	 * Serial number
	 */
	private static final long serialVersionUID = 8584975439432041087L;
	/**
	 * Error text message.
	 */
	private String error;
	
	/**
	 * Constructor of the fail message.
	 * @param error Error text message.
	 */
	public FailMsg(String error){
		this.error = error;
	}
	
	/**
	 * Get the fail message.
	 * @return fail text message.
	 */
	@Override
	public String getErrorText() {
		return error;
	}

}

