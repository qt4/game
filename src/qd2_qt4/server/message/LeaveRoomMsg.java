package qd2_qt4.server.message;

import common.ILeaveRoomMsg;
import common.IMember;

/**
 * A known message, leave room message.
 * @author qt4 
 *
 */
public class LeaveRoomMsg implements ILeaveRoomMsg{

	/**
	 * Serial number
	 */
	private static final long serialVersionUID = 8871948310615801830L;
	/**
	 * Member stub who leaves the room
	 */
	private IMember myMember;
	
	/**
	 * Constructor of the message.
	 * @param member Member stub who leaves the room
	 */
	public LeaveRoomMsg(IMember member){
		this.myMember = member;
	}
	
	/**
	 * Get member stub who leaves the room
	 * @return Member stub who leaves the room.
	 */
	@Override
	public IMember getMember() {
		return myMember;
	}

}
