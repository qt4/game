package qd2_qt4.server.message;

import common.IRejectMsg;

/**
 * A known message, reject message.
 * @author qt4
 *
 */
public class RejectMsg implements IRejectMsg{

	/**
	 * Serial number
	 */
	private static final long serialVersionUID = 8663915246521570353L;
	/**
	 * Reject reason.
	 */
	private String reason;
	
	/**
	 * Constructor of reject message.
	 * @param reason Reject reason.
	 */
	public RejectMsg(String reason){
		this.reason = reason;
	}
	
	/**
	 * Get reject reason
	 * @return Reject reason.
	 */
	@Override
	public String getReasonText() {
		return reason;
	}

}
