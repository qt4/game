package qd2_qt4.server.message;

import common.ISuccessMsg;

/**
 * A known message, success message.
 * @author qt4
 *
 */
public class SuccessMsg implements ISuccessMsg{

	/**
	 * Serial number.
	 */
	private static final long serialVersionUID = -1987736909132475631L;

}
