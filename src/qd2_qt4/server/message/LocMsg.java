package qd2_qt4.server.message;

import java.io.Serializable;

/**
 * A game message, show the location of a player.
 * @author qt4
 *
 */
public class LocMsg implements Serializable{

	/**
	 * Serial number
	 */
	private static final long serialVersionUID = -2634952387021497469L;

	/**
	 * Current latitude.
	 */
	private double curLat;
	/**
	 * Current longitude.
	 */
	private double curLon;
	
	/**
	 * Destination latitude
	 */
	private double desLat;
	/**
	 * Destination longitude.
	 */
	private double desLon;
	/**
	 * A flag whether a player is on board
	 */
	private boolean onBoard;
	
	
	/**
	 * Location message constructor
	 * @param degrees Original latitude.
	 * @param degrees2 Original longitude.
	 * @param degrees3 Destination latitude.
	 * @param degrees4 Destination longitude.
	 * @param onBoard If a player is on board.
	 */
	public LocMsg(double degrees, double degrees2, double degrees3, double degrees4, boolean onBoard){
		this.curLat = degrees;
		this.curLon = degrees2;
		this.desLat = degrees3;
		this.desLon = degrees4;
		this.onBoard = onBoard;
	}
	
	/**
	 * Get the flag.
	 * @return If a player is on board.
	 */
	public boolean getOnBoard(){
		return onBoard;
	}
	
	/**
	 * Get current latitude.
	 * @return Current latitude.
	 */
	public double getCurLat(){
		return curLat;
	}
	
	/**
	 * Get current longitude.
	 * @return Current longitude.
	 */
	public double getCurLon(){
		return curLon;
	}
	
	/**
	 * Get destination latitude.
	 * @return Destination latitude.
	 */
	public double getDesLat(){
		return desLat;
	}
	
	/**
	 * Get destination longitude.
	 * @return Destination longitude.
	 */
	public double getDesLon(){
		return desLon;
	}
}
