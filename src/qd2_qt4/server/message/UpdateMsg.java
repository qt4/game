package qd2_qt4.server.message;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * A game message, to update the scores.
 * @author qt4
 *
 */
public class UpdateMsg implements Serializable{

	/**
	 * Serial number
	 */
	private static final long serialVersionUID = -2669303608792960122L;
	
	/**
	 * Score list.
	 */
	private ArrayList<String> scoreList;
	
	/**
	 * Score message.
	 */
	private String hint;
	
	/**
	 * Constructor of the message.
	 * @param scoreList Score list.
	 * @param hint Score message.
	 */
	public UpdateMsg(ArrayList<String> scoreList, String hint) {
		this.scoreList = scoreList;
		this.hint = hint;
	}
	
	/**
	 * Get the score list.
	 * @return Score list.
	 */
	public ArrayList<String> getScoreList(){
		return scoreList;
	}
	
	/**
	 * Get the score message.
	 * @return Score message.
	 */
	public String getHint(){
		return hint;
	}
}
