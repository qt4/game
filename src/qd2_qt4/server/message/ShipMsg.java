package qd2_qt4.server.message;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.UUID;

/**
 * A game message, to inform clients to show ships.
 * @author qt4
 *
 */
public class ShipMsg implements Serializable{

	/**
	 * Serial number
	 */
	private static final long serialVersionUID = -5073387828877496210L;
	
	/**
	 * UUIDs of ships to show.
	 */
	private ArrayList<UUID> shipUUID;
	
	/**
	 * Constructor of the message.
	 * @param shipUUID Ship UUIDs to show.
	 */
	public ShipMsg(ArrayList<UUID> shipUUID){
		this.shipUUID = shipUUID;
	}
	
	/**
	 * Get the UUIDs of ships
	 * @return UUIDs of ships.
	 */
	public ArrayList<UUID> getShipUUID(){
		return shipUUID;
	}
}
