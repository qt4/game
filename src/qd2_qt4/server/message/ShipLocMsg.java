package qd2_qt4.server.message;

import java.io.Serializable;

/**
 * A game message, to pass the positions of a ship.
 * @author qt4
 *
 */
public class ShipLocMsg implements Serializable{

	/**
	 * Serial number
	 */
	private static final long serialVersionUID = 2867576780943830239L;
	/**
	 * Ship's current latitude.
	 */
	private double curLat;
	/**
	 * Ship's current longitude.
	 */
	private double curLon;
	/**
	 * Ship's destination latitude.
	 */
	private double desLat;
	/**
	 * Ship's destination longitude.
	 */
	private double desLon;
	
	
	
	/**
	 * Constructor of the message.
	 * @param degrees Ship's current latitude.
	 * @param degrees2 Ship's current longitude.
	 * @param degrees3 Ship's destination latitude.
	 * @param degrees4 Ship's destination longitude.
	 */
	public ShipLocMsg(double degrees, double degrees2, double degrees3, double degrees4){
		this.curLat = degrees;
		this.curLon = degrees2;
		this.desLat = degrees3;
		this.desLon = degrees4;
	}
	
	/**
	 * Get ship's current latitude.
	 * @return Current latitude.
	 */
	public double getCurLat(){
		return curLat;
	}
	/**
	 * Get ship's current longitude.
	 * @return Current longitude.
	 */
	public double getCurLon(){
		return curLon;
	}
	/**
	 * Get ship's destination latitude.
	 * @return Destination latitude.
	 */
	public double getDesLat(){
		return desLat;
	}
	/**
	 * Get ship's destination longitude.
	 * @return Destination longitude.
	 */
	public double getDesLon(){
		return desLon;
	}
}
