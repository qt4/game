package qd2_qt4.server.view;

import java.util.Collection;
import java.util.Vector;

import common.IPerson;

/**
 * Main view to model adapter.
 * @author qd2, qt4
 *
 */
public interface IModelAdapter {
	/**
	 * Connect to given host or IP address.
	 * @param s Host name or IP address
	 * @return Status string.
	 */
	public String connectTo(String s);
	/**
	 * get IPerson stubs that connect to me
	 * @return collection of IPersons that connects to me
	 */
	public Collection<IPerson> getConnection();

	/**
	 * Make a team.
	 * @param name Team name
	 */
	public void makeChatroom(String name);
	/**
	 * Invite selected player to a selected team.
	 * @param selectedPlayer Selected player.
	 * @param team Selected team.
	 */
	public void invite(int[] selectedPlayer, int team);
	/**
	 * Start game for a selected team.
	 * @param selectedTeam Selected team.
	 */
	public void startGame(int selectedTeam);
	/**
	 * Get team list.
	 * @return team list.
	 */
	public Vector<String> getTeam();
	/**
	 * Get team members.
	 * @param selectedChatroom Selected team.
	 * @return Team members.
	 */
	public Vector<String> getMembers(int selectedChatroom);
	/**
	 * Stop game for all teams.
	 */
	public void stopGame();
	/**
	 * Quit the server.
	 */
	public void quit();
}
