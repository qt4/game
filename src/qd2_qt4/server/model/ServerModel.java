package qd2_qt4.server.model;

import java.awt.Component;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.Vector;

import javax.swing.JOptionPane;

import qd2_qt4.game.Chatroom;
import qd2_qt4.game.data.GameData;
import qd2_qt4.server.message.StartGameMsg;
import qd2_qt4.server.message.StopGameMsg;
import common.IChatroom;
import common.ICmd2ModelAdapter;
import common.IMember;
import common.IPerson;
import provided.datapacket.ADataPacket;
import provided.datapacket.DataPacket;
import provided.mixedData.IMixedDataDictionary;
import provided.mixedData.MixedDataDictionary;
import provided.rmiUtils.*;
import provided.util.IVoidLambda;


/**
 * Main model of the application.
 * @author qd2_qt4
 *
 */
public class ServerModel {
	/**
	 * Model to view adapter.
	 */
	private IViewAdapter view;
	
	/**
	 * Local member object.
	 */
	private Member myMember;
	
	/**
	 * Local member stub
	 */
	private ArrayList<IMember> myMemberStubs = new ArrayList<IMember>();
	
	/**
	 * Local person object.
	 */
	private IPerson myPerson;
	/**
	 * Person name.
	 */
	private String myPersonName;
	/**
	 * Local person stub.
	 */
	private IPerson localPersonStub;
	/**
	 * RMI registry to store person stub
	 */
	private Registry registry;
	/**
	 * Person UUID.
	 */
	private UUID myUUID;
	/**
	 * Mixed data dictionary.
	 */
	private IMixedDataDictionary dataDict = new MixedDataDictionary();
	
	/**
	 * Connected user list.
	 */
	private ArrayList<IPerson> myConnection;

	/**
	 * Chatroom list
	 */
	private List<IChatroom> chatroomList = new ArrayList<IChatroom>();
	
	/**
	 * Chat room name list.
	 */
	private Vector<String> roomNameList = new Vector<String>();
	
	/**
	 * Member list
	 */
	private Vector<String> memberList = new Vector<String>();
	
	/**
	 * A team includes all players.
	 */
	private IChatroom allTeam;
	/**
	 * A command used as a wrapper around the view adapter for the IRMIUtils and the ComputeEngine.
	 */
	private IVoidLambda<String> outputCmd = new IVoidLambda<String> (){
		public void apply(String... strs){
			for(String s: strs)view.append(s);
		}
	};	
	
	/**
	 * RMI utility.
	 */
	private IRMIUtils rmiUtils = new RMIUtils(outputCmd);

	

	
	/**
	 * Command to model adapter implementation.
	 */
	private ICmd2ModelAdapter cmd2ModelAdapter = new ICmd2ModelAdapter(){

		@Override
		public IMember getLocalMember() {
//			return myMemberStub;
			return myMember;
		}

		@Override
		public void displayStringMsg(String s) {
			view.append(s);
			
		}

		@Override
		public void displayComponentMsg(String name, Component comp) {
			// reject to show
			
		}

		@Override
		public void addGameComponent(String name, Component comp) {
			// reject to add
		}

		@Override
		public void send2Member(IMember member, ADataPacket msg) {
			// reject
		}

		@Override
		public void send2Team(ADataPacket msg) {
//			reject
		}

		@Override
		public IMixedDataDictionary getMDD() {
			return dataDict;
		}};
	
	
	
	/**
	 * Constructor of the model.
	 * @param view Model to view adapter.
	 */
	public ServerModel(IViewAdapter view) {
		this.view = view;
		this.myConnection = new ArrayList<IPerson>();
		this.myUUID = UUID.randomUUID();
		
		/**
		 * Top level stub. 
		 */
		myPerson = new IPerson() {
			
		
			@Override
			public int connectBack(IPerson myPersonStub) throws RemoteException {
				Iterator<IPerson> iterator = myConnection.iterator();
				
				while (iterator.hasNext()) {
					IPerson p = iterator.next();
					try {
						if (p.getUUID().equals(myPersonStub.getUUID())) return STATUS_SUCC;
					} catch (Exception e) {
						e.printStackTrace();
						view.append("Exception caught when searching for IPerson stub.\nThe person might have been disconnected.");
						iterator.remove();
					}
				}
				
				myConnection.add(myPersonStub);
				view.append("Connection to " + myPersonStub.getName() + " established!\n");
				view.updateConnectedList();
				return STATUS_SUCC;
			}

			@Override
			public int acceptInvitation(IChatroom chatroom)
					throws RemoteException {
				//createMiniMVC();
				return 0;
			}

			@Override
			public int recvRequest(IPerson requesterStub)
					throws RemoteException {
				if (requesterStub.acceptInvitation(chatroomList.get(0))==IPerson.STATUS_SUCC)				
					view.append("Invitation to Lobby succeeded.\n");
				
				return IPerson.STATUS_SUCC;
			}

			@Override
			public String getName() throws RemoteException {
				return myPersonName;
			}

			@Override
			public UUID getUUID() throws RemoteException {
				return myUUID;
			}
		};
				
	}
	
	/**
	 * Start the model.
	 */
	public void start() {
		rmiUtils.startRMI(IRMI_Defs.CLASS_SERVER_PORT_SERVER);
		
		try {
			String name = view.inputWindow("Input your name: ", System.getProperty("user.name"));
			if (name==null) System.exit(0);
			this.myPersonName = name;
			localPersonStub = (IPerson) UnicastRemoteObject.exportObject(myPerson, IPerson.SERVER_BOUND_PORT);
			view.append("Looking for registry..."+"\n");
			
			registry = rmiUtils.getLocalRegistry();
			
			view.append("Found registry: " + registry + "\n");
			registry.rebind(IPerson.SERVER_BOUND_NAME, localPersonStub);
			view.append("IPerson bound to " + IPerson.SERVER_BOUND_NAME + "\n");
		}
		catch(Exception e) {
			System.err.println("IPerson exception:" + "\n");
			e.printStackTrace();
			System.exit(-1);
		}
		
		//Make Lobby
		makeChatroom("Lobby");
		
		view.append("Waiting..."+"\n");
	}
	

	/**
	 * Connect to a remote host or IP address.
	 * @param remoteHost Host name or IP.
	 * @return A result status string.
	 */
	public String connectTo(String remoteHost) {
		try {
			Thread thread = new Thread() {
				public void run() {
					try {
						view.append("Locating registry at " + remoteHost + "...\n");
						Registry registry = rmiUtils.getRemoteRegistry(remoteHost);
						view.append("Found registry: " + registry + "\n");
						IPerson remoteP = (IPerson) registry.lookup(IPerson.SERVER_BOUND_NAME);
						view.append("Found remote IPerson object: " + remoteP + "\n");
						myPerson.connectBack(remoteP);		
						remoteP.connectBack(myPerson);
						view.updateConnectedList();
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
			};
			thread.start();
		} catch(Exception e) {
			view.append("Exception connecting to " + remoteHost + ": " + e + "\n");
			e.printStackTrace();
			return "No connection established!";
		}
		return "Connection to " + remoteHost + "  established!";
	}

	/**
	 * Get all the connected user stub.
	 * @return A collection of all the stubs.
	 */
	public Collection<IPerson> getConnection() {
		return this.myConnection;
	}
	
	
	public Vector<String> getChatroom(){
		return roomNameList;
	}
	
	public Vector<String> getMembers(int selectedChatroom){
		memberList.clear();
		for (IMember member: chatroomList.get(selectedChatroom).getMembers()) {
			try {
				memberList.add(member.getName());
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
		return memberList;
	}
	
	/**
	 * Requeste to join a chatroom.
	 * @param idx The person being requested.
	 */
	public void request(int idx) {
		try {
			int res = myConnection.get(idx).recvRequest(localPersonStub);
			if (res == IPerson.STATUS_FAILED) {
				JOptionPane.showMessageDialog(null, "The request is rejected");
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Make a team.
	 * @param name Team name.
	 */
	public void makeChatroom(String name) {
		IChatroom chat = new Chatroom(name);
		chatroomList.add(chat);
		roomNameList.add(name);

		System.out.println("chatroomList size: "+chatroomList.size());
		myMember = new Member(myUUID, chat, cmd2ModelAdapter);
		System.out.println("Created a new Team, and instantiated a new Server IMember");
		IMember myMemberStub = null;
		try {
			myMemberStub = (IMember) UnicastRemoteObject.exportObject(myMember, 2012);
			chat.addMember(myMemberStub);
			myMemberStubs.add(myMemberStub);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		view.updateTeamList();
		
	}

	/**
	 * Invite selected player to a team.
	 * @param selectedPlayer Selected player index.
	 * @param team Selected team index.
	 */
	public void invite(int[] selectedPlayer, int team) {
		try {
			for (int i: selectedPlayer){
				int res = myConnection.get(i).acceptInvitation(chatroomList.get(team));
				if (res == IPerson.STATUS_FAILED) {
					JOptionPane.showMessageDialog(null, "The invitation is rejected");
				} else {
					view.append("Invitation succeeded!\n");
				}
			}
			
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Start game for a given team.
	 * @param selectedTeam Selected team.
	 */
	public void startGame1(int selectedTeam) {
		//create a AllTeam
		if (allTeam==null) {
			allTeam = new Chatroom("All Team");
			myMember = new Member(myUUID, allTeam, cmd2ModelAdapter);
			System.out.println("Created a All Team");
			IMember myMemberStub = null;
			try {
				myMemberStub = (IMember) UnicastRemoteObject.exportObject(myMember, 2012);
				allTeam.addMember(myMemberStub);
				
				for (int i=1; i<chatroomList.size(); i++){
					for (IMember member: chatroomList.get(i).getMembers()){
						allTeam.addMember(member);
					}
				}
				
				for (IMember member: myMemberStubs){
					allTeam.removeMember(member);
				}
				
			} catch (RemoteException e) {
				e.printStackTrace();
			}
			
		}
		
		IMember me = null;
		IChatroom team = chatroomList.get(selectedTeam);
		Collection<IMember> members = team.getMembers();
		System.out.println("Chatroom member size: "+members.size());

		for (IMember member: members){
			try {
				if (member.getUUID().equals(myUUID)){
					me = member;
					System.out.println("Chatroom member: "+member.getName());
				}
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
		
		team.broadcastMsg(new DataPacket<StartGameMsg>(StartGameMsg.class, me, new StartGameMsg(GameData.Singleton.getAlarms(), allTeam, randomInt(1,5))));
		
		//init team score
		GameData.Singleton.initData();
		GameData.Singleton.setTeamScore(team.getUUID(), 0);
		GameData.Singleton.setTeamKey(team.getUUID());
		GameData.Singleton.setTeamName(team.getUUID(), team.getName());
		GameData.Singleton.addTeam(team.getUUID(), team);

	}

	
	/**
	 * Start game of selected game with multithread.
	 * @param selectedTeam Selected team
	 */
	public void startGame(int selectedTeam) {
		Thread startThread = new Thread( new Runnable() {
			public void run() {
				startGame1(selectedTeam);
			}
		});
		startThread.start();
	}
	
	
	
	
	/**
	 * Random integer generator
	 * @param min Mininum of the range.
	 * @param max Maximum of the range.
	 * @return A random integer within the range.
	 */
	public int randomInt(int min, int max) {
		return (int)Math.floor((Math.random()*(1+max-min))+min);
	}

	/**
	 * Stop game for all teams.
	 */
	public void stopGame() {
		IMember me = null;
		for (IMember member: allTeam.getMembers()){
			try {
				if (!member.getUUID().equals(myUUID)){
					me = member;
				}
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
		
		Map<UUID, Integer> teamScores = GameData.Singleton.getTeamScores();
		Set<UUID> keys = teamScores.keySet();
		int max = 0;
		ArrayList<UUID> maxUUIDs = new ArrayList<UUID>();
		for (UUID uuid: keys){
			if (teamScores.get(uuid)>max){
				max = teamScores.get(uuid);
			}
		}
		
		for (UUID uuid: keys){
			if (teamScores.get(uuid)==max){
				maxUUIDs.add(uuid);
			}
		}
		
		String stopMsg = "";
		for (UUID uuid: maxUUIDs){
			String winTeam = GameData.Singleton.getTeamName().get(uuid);
			stopMsg = stopMsg + "Team: "+ winTeam + ", score: " + max +".\n";
		}
		
		
		
		allTeam.broadcastMsg(new DataPacket<StopGameMsg>(StopGameMsg.class, me, 
				new StopGameMsg("The winner(s): "+ stopMsg)));
	}
	
	
	/**
	 * Stops the EngineModel by unbinding the ComputeEngine from the Registry 
	 * and stopping class file server. 
	 */
	public void stop() {
		try {
			registry.unbind(IPerson.SERVER_BOUND_NAME);
			System.out.println("Server Controller: " + IPerson.SERVER_BOUND_NAME
					+ " has been unbound.");

			rmiUtils.stopRMI();

			System.exit(0);
		} catch (Exception e) {
			System.err.println("Server Controller: Error unbinding "
					+ IPerson.SERVER_BOUND_NAME + ":\n" + e);
			System.exit(-1);
		}
	}
	
}
