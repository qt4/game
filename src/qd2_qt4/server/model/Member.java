package qd2_qt4.server.model;

import java.rmi.RemoteException;
import java.util.UUID;

import provided.datapacket.ADataPacket;
import provided.datapacket.ADataPacketAlgoCmd;
import provided.datapacket.DataPacket;
import provided.datapacket.DataPacketAlgo;
import qd2_qt4.server.message.SuccessMsg;
import qd2_qt4.server.algo.MsgAlgo;
import common.IChatroom;
import common.ICmd2ModelAdapter;
import common.IJoinRoomMsg;
import common.ILeaveRoomMsg;
import common.IMember;
import common.ISuccessMsg;

/**
 * Member object used by server.
 * @author qt4
 *
 */
public class Member implements IMember{
	/**
	 * Member name
	 */
	private transient String memberName = "server";//System.getProperty("user.name");

	/**
	 * Team 
	 */
	private transient IChatroom chatroom;

	/**
	 * Member UUID
	 */
	private UUID uuid;

	/**
	 * Algorithm used by the member.
	 */
	private transient DataPacketAlgo<ADataPacket, Void> algo;

	/**
	 * Command to model adapter.
	 */
	private transient ICmd2ModelAdapter cmd2ModelAdpt;

	/**
	 * Constructor of Member.
	 * @param uuid UUID of the member.
	 * @param chatroom Team of the member.
	 * @param cmd2ModelAdpt Command to model adapter.
	 */
	public Member(UUID uuid, IChatroom chatroom, ICmd2ModelAdapter cmd2ModelAdpt){
		this.uuid = uuid;
		this.chatroom = chatroom;
		this.cmd2ModelAdpt = cmd2ModelAdpt;
		this.algo = newAlgo();
	}

	/**
	 * Set the command to model adapter.
	 * @param cmd2ModelAdpt Command to model adapter to set.
	 */
	public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
		this.cmd2ModelAdpt = cmd2ModelAdpt;

	}

	/**
	 * Get member name.
	 * @return Name of the member.
	 */
	@Override
	public String getName() throws RemoteException {
		return memberName;
	}
	/**
	 * Get member UUID.
	 * @return UUID of the member.
	 */
	@Override
	public UUID getUUID() throws RemoteException {
		return uuid;
	}

	/**
	 * Receive message.
	 * @param msg Message received.
	 * @return Message processed results.
	 */
	@Override
	public ADataPacket recvMessage(ADataPacket msg) throws RemoteException {
		return msg.execute(algo, new Void[0]);
	}

	/**
	 * Make a new algorithm instance.
	 * @return A new algo instance.
	 */
	private DataPacketAlgo<ADataPacket, Void> newAlgo(){

		MsgAlgo msgAlgo = new MsgAlgo(null, cmd2ModelAdpt, chatroom);

		DataPacketAlgo<ADataPacket, Void> newAlgo = msgAlgo;

		/**
		 * Join message
		 */
		newAlgo.setCmd(IJoinRoomMsg.class, new ADataPacketAlgoCmd<ADataPacket, IJoinRoomMsg, Void>(){

			/**
			 * Serial number
			 */
			private static final long serialVersionUID = 1213071532179293924L;

			@Override
			public ADataPacket apply(Class<?> index,
					DataPacket<IJoinRoomMsg> host, Void... params) {
				chatroom.addMember(host.getSender());
				System.out.println("Chatroom member list size: "+ chatroom.getMembers().size());
				try {
					System.out.println("New member name: "+ host.getSender().getName());
				} catch (RemoteException e) {
					e.printStackTrace();
				}
				return new DataPacket<ISuccessMsg>(ISuccessMsg.class, Member.this, new SuccessMsg());
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				Member.this.cmd2ModelAdpt = cmd2ModelAdpt;
			}

		});


		/**
		 * Leave message
		 */
		newAlgo.setCmd(ILeaveRoomMsg.class, new ADataPacketAlgoCmd<ADataPacket, ILeaveRoomMsg, Void>(){


			/**
			 * Serial number
			 */
			private static final long serialVersionUID = 7494248407659974865L;

			@Override
			public ADataPacket apply(Class<?> index,
					DataPacket<ILeaveRoomMsg> host, Void... params) {
				try {
					System.out.println(cmd2ModelAdpt.getLocalMember().getName() + " received LeaveMsg from: " + host.getSender().getName());
					System.out.println("Leaving member name: "+ host.getData().getMember());
				} catch (RemoteException e) {
					e.printStackTrace();
				}
//				if (!host.getData().getMember().equals(host.getSender()))
//					chatroom.removeMember(host.getData().getMember());
				chatroom.removeMember(host.getSender());
				return new DataPacket<ISuccessMsg>(ISuccessMsg.class, Member.this, new SuccessMsg());
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				Member.this.cmd2ModelAdpt = cmd2ModelAdpt;
			}

		});



		return newAlgo;
	}

}
