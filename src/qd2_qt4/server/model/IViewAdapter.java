package qd2_qt4.server.model;

/**
 * Main model to view adapter.
 * @author qd2, qt4
 *
 */
public interface IViewAdapter {
	/**
	 * Append text to the view window.
	 * @param s The text to show.
	 */
	public void append(String s);

	/**
	 * choice window for user to choose
	 * @param choices items for user to choose from 
	 * @param message message shown in the choice window
	 * @param title title of the choice window
	 * @return item that user chooses
	 */
	public String choiceWindow(String[] choices, String message, String title);

	/**
	 * input window for user to input
	 * @param message message shown in the input window
	 * @param defaultValue Default value for the window
	 * @return string that user inputs
	 */
	public String inputWindow(String message, String defaultValue);

	/**
	 * yes or no window for user 
	 * @param message message shown in the yes or no window
	 * @param title title of the window
	 * @return choice user makes
	 */
	public int yesNoWindow(String message, String title);

	/**
	 * Update the connected list on the main view.
	 */
	public void updateConnectedList();

	/**
	 * Update team list.
	 */
	public void updateTeamList();

}
