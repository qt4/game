package qd2_qt4.server.algo;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

import common.IChatroom;
import common.ICmd2ModelAdapter;
import common.IFailMsg;
import common.IMember;
import common.IRejectMsg;
import common.IRequestCmd;
import common.ISuccessMsg;
import common.ITextMsg;
import common.IAddCmd;
import provided.datapacket.ADataPacket;
import provided.datapacket.ADataPacketAlgoCmd;
import provided.datapacket.DataPacket;
import provided.datapacket.DataPacketAlgo;
import provided.extvisitor.IExtVisitorCmd;
import qd2_qt4.game.data.GameData;
import qd2_qt4.server.message.AddCmd;
import qd2_qt4.server.message.CollectMsg;
import qd2_qt4.server.message.LocMsg;
import qd2_qt4.server.message.ShipLocMsg;
import qd2_qt4.server.message.ShipMsg;
import qd2_qt4.server.message.StartGameMsg;
import qd2_qt4.server.message.StopGameMsg;
import qd2_qt4.server.message.SuccessMsg;
import qd2_qt4.server.message.UpdateMsg;

/**
 * Server message algorithm
 * @author qt4
 *
 */
public class MsgAlgo extends DataPacketAlgo<ADataPacket, Void> {

	/**
	 * serial number
	 */
	private static final long serialVersionUID = 762868564162394668L;
	/**
	 * Command to model adapter
	 */
	private transient ICmd2ModelAdapter cmd2ModelAdpt;

	/**
	 * Constructor
	 * @param defaultCmd Default command
	 * @param cmd2MAdpt Command to model adapter.
	 * @param team Team
	 */
	public MsgAlgo(ADataPacketAlgoCmd<ADataPacket, Object, Void> defaultCmd, 
			ICmd2ModelAdapter cmd2MAdpt, IChatroom team) {
		super(defaultCmd);

		this.cmd2ModelAdpt = cmd2MAdpt;
//		this.team = team;

		/**
		 *  set TextMsg Command.
		 */
		this.setCmd(ITextMsg.class, new ADataPacketAlgoCmd<ADataPacket, ITextMsg, Void>(){

			/**
			 * Serial number
			 */
			private static final long serialVersionUID = 4488472418196793722L;

			@Override
			public ADataPacket apply(Class<?> index, DataPacket<ITextMsg> host,
					Void... params) {
				String text = host.getData().getText();
				String senderName = " ";
				try {
					senderName = host.getSender().getName();
				} catch (RemoteException e) {
					e.printStackTrace();
				}
				MsgAlgo.this.cmd2ModelAdpt.displayStringMsg(new Date().toString()+"\n"+senderName+" says: "+text);
				System.out.println(text);
				return new DataPacket<ISuccessMsg>(ISuccessMsg.class, cmd2ModelAdpt.getLocalMember(), new SuccessMsg());
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				MsgAlgo.this.cmd2ModelAdpt = cmd2ModelAdpt;
			}

		});



		/**
		 *  Set RequestCmd.
		 */
		this.setCmd(IRequestCmd.class, new ADataPacketAlgoCmd<ADataPacket, IRequestCmd, Void>(){

			/**
			 * Serial number
			 */
			private static final long serialVersionUID = 2313399919455080516L;

			@SuppressWarnings("unchecked")
			@Override
			public ADataPacket apply(Class<?> index, DataPacket<IRequestCmd> host,
					Void... params) {


				Class<?> idx = host.getData().getID();		
				ADataPacketAlgoCmd<ADataPacket, ?, Void> cmd = (ADataPacketAlgoCmd<ADataPacket, ?, Void>) (new GameAlgo(null, cmd2ModelAdpt, team).getCmd(idx));

				try {
					System.out.println(cmd2ModelAdpt.getLocalMember().getName() + " received RequestCmd from: " + host.getSender().getName());
					System.out.println("Cmd ID is: " + host.getData().getID());
					System.out.println("Cmd2ModelAdpater is: " + cmd2ModelAdpt);
				} catch (RemoteException e) {
					e.printStackTrace();
				}
				return new DataPacket<IAddCmd>(IAddCmd.class, cmd2ModelAdpt.getLocalMember(), new AddCmd(idx, cmd));
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				MsgAlgo.this.cmd2ModelAdpt = cmd2ModelAdpt;
			}

		});

		/**
		 *  Set AddCmd.
		 */
		this.setCmd(IAddCmd.class, new ADataPacketAlgoCmd<ADataPacket, IAddCmd, Void>(){

			/**
			 * Serial number
			 */
			private static final long serialVersionUID = -1655939950799464594L;

			@Override
			public ADataPacket apply(Class<?> index, DataPacket<IAddCmd> host,
					Void... params) {

				try {
					System.out.println(cmd2ModelAdpt.getLocalMember().getName() + " received Add cmd from: " + host.getSender().getName());
					System.out.println("Cmd ID is: " + host.getData().getID());
					System.out.println("Cmd2ModelAdpater is: " + cmd2ModelAdpt);
				} catch (RemoteException e) {
					e.printStackTrace();
				}

				MsgAlgo.this.setCmd(host.getData().getID(), (IExtVisitorCmd<ADataPacket, Class<?>, Void, ADataPacket>) host.getData().getCmd());
				return new DataPacket<ISuccessMsg>(ISuccessMsg.class, cmd2ModelAdpt.getLocalMember(), new SuccessMsg());
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				MsgAlgo.this.cmd2ModelAdpt = cmd2ModelAdpt;
			}



		});


		/**
		 *  success message
		 */
		this.setCmd(ISuccessMsg.class, new ADataPacketAlgoCmd<ADataPacket, ISuccessMsg, Void>(){


			/**
			 * Serial number
			 */
			private static final long serialVersionUID = -448317610685305867L;

			@Override
			public ADataPacket apply(Class<?> index, DataPacket<ISuccessMsg> host,
					Void... params) {
				return new DataPacket<ISuccessMsg>(ISuccessMsg.class, cmd2ModelAdpt.getLocalMember(), new SuccessMsg());

			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				MsgAlgo.this.cmd2ModelAdpt = cmd2ModelAdpt;
			}

		});


		/**
		 * 	fail message
		 */
		this.setCmd(IFailMsg.class, new ADataPacketAlgoCmd<ADataPacket, IFailMsg, Void>(){


			/**
			 * Serial number
			 */
			private static final long serialVersionUID = -6118775019987374522L;

			@Override
			public ADataPacket apply(Class<?> index, DataPacket<IFailMsg> host,
					Void... params) {
				return new DataPacket<ISuccessMsg>(ISuccessMsg.class, cmd2ModelAdpt.getLocalMember(), new SuccessMsg());

			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				MsgAlgo.this.cmd2ModelAdpt = cmd2ModelAdpt;
			}

		});

		/**
		 * 	Start game message
		 */
		this.setCmd(StartGameMsg.class, new ADataPacketAlgoCmd<ADataPacket, StartGameMsg, Void>(){

			/**
			 * Serial number
			 */
			private static final long serialVersionUID = 665365629345268244L;

			@Override
			public ADataPacket apply(Class<?> index, DataPacket<StartGameMsg> host,
					Void... params) {
				return new DataPacket<ISuccessMsg>(ISuccessMsg.class, cmd2ModelAdpt.getLocalMember(), new SuccessMsg());
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				MsgAlgo.this.cmd2ModelAdpt = cmd2ModelAdpt;
			}


		});



		/**
		 * 	location message
		 */
		this.setCmd(LocMsg.class, new ADataPacketAlgoCmd<ADataPacket, LocMsg, Void>(){

			/**
			 * Serial number
			 */
			private static final long serialVersionUID = -2090275446204592328L;

			@Override
			public ADataPacket apply(Class<?> index, DataPacket<LocMsg> host,
					Void... params) {
				double curLat = host.getData().getCurLat();
				double curLon = host.getData().getCurLon();
				double desLat = host.getData().getDesLat();
				double desLon = host.getData().getDesLon();
				try {

					System.out.println(host.getSender().getName());
					System.out.println("Current Lattitude is "+ curLat +". Longitude is " + curLon);
					System.out.println("Destination Lattitude is "+ desLat +". Longitude is " + desLon);


				} catch (RemoteException e) {

					e.printStackTrace();
				}

				return new DataPacket<ISuccessMsg>(ISuccessMsg.class, cmd2ModelAdpt.getLocalMember(), new SuccessMsg());
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				MsgAlgo.this.cmd2ModelAdpt = cmd2ModelAdpt;
			}

		});


		/**
		 * 	Collect message
		 */
		this.setCmd(CollectMsg.class, new ADataPacketAlgoCmd<ADataPacket, CollectMsg, Void>(){

			/**
			 * Serial number
			 */
			private static final long serialVersionUID = 7440923604962910485L;

			@Override
			public ADataPacket apply(Class<?> index,
					DataPacket<CollectMsg> host, Void... params) {
				
				StringBuilder sb = new StringBuilder();
				UUID teamUUID = host.getData().getTeamUUID();
				String teamName = GameData.Singleton.getTeamName().get(teamUUID);
				UUID pointID = host.getData().getIcon();

				// found a key, set key number
				if (GameData.Singleton.getKeyPoints().containsKey(pointID)){
					GameData.Singleton.setTeamKey(teamUUID);
					System.out.println(GameData.Singleton.getTeamKeys().get(teamUUID));
					sb.append("Team " + teamName + " got a key point (30 pts).\n");
					
					// if to show ship
					if (GameData.Singleton.getTeamKeys().get(teamUUID)==4) {
						System.out.println("KeyPoint latitude: "+GameData.Singleton.getKeyPoints().get(pointID).get(0));
						System.out.println("KeyPoint longitude: "+GameData.Singleton.getKeyPoints().get(pointID).get(1));


						//show all ships in a team
						ArrayList<UUID> shipUUID = new ArrayList<UUID>();
						
						for (IMember member: GameData.Singleton.getTeam(teamUUID).getMembers()){
							try {
								UUID uuid = member.getUUID();
								if (!uuid.equals(cmd2ModelAdpt.getLocalMember().getUUID()))
									shipUUID.add(member.getUUID());
							} catch (RemoteException e) {
								e.printStackTrace();
							}
						}
						
						sb.append("Team " + teamName + " got ships.\n");
						team.broadcastMsg(new DataPacket<ShipMsg>(ShipMsg.class, cmd2ModelAdpt.getLocalMember(), new ShipMsg(shipUUID)));
					} 

				} else {
					sb.append("Team " + teamName + " got 10 pts.\n");
				}
				// set score
				
				System.out.println("Point score: "+GameData.Singleton.getAlarms().get(pointID).get(2).intValue());
				GameData.Singleton.setTeamScore(teamUUID, GameData.Singleton.getAlarms().get(pointID).get(2).intValue());

				//send score
				
				team.broadcastMsg(new DataPacket<UpdateMsg>(UpdateMsg.class, cmd2ModelAdpt.getLocalMember(), new UpdateMsg(GameData.Singleton.getScores(), sb.toString())));
				return new DataPacket<ISuccessMsg>(ISuccessMsg.class, cmd2ModelAdpt.getLocalMember(), new SuccessMsg());
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				MsgAlgo.this.cmd2ModelAdpt = cmd2ModelAdpt;
			}

		});


		/**
		 * 	Show ship message
		 */
		this.setCmd(ShipMsg.class, new ADataPacketAlgoCmd<ADataPacket, ShipMsg, Void>(){

			/**
			 * Serial number
			 */
			private static final long serialVersionUID = 7195737341408368710L;

			@Override
			public ADataPacket apply(Class<?> index,
					DataPacket<ShipMsg> host, Void... params) {

				try {
					System.out.println(cmd2ModelAdpt.getLocalMember().getName() + " received ShipMsg from: " + host.getSender().getName());
				} catch (RemoteException e) {

					e.printStackTrace();
				}

				return new DataPacket<ISuccessMsg>(ISuccessMsg.class, cmd2ModelAdpt.getLocalMember(), new SuccessMsg());
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				MsgAlgo.this.cmd2ModelAdpt = cmd2ModelAdpt;
			}

		});



		/**
		 * 	Update scores message
		 */
		this.setCmd(UpdateMsg.class, new ADataPacketAlgoCmd<ADataPacket, UpdateMsg, Void>(){

			/**
			 * Serial number
			 */
			private static final long serialVersionUID = 6999934322134269562L;

			@Override
			public ADataPacket apply(Class<?> index,
					DataPacket<UpdateMsg> host, Void... params) {
				return new DataPacket<ISuccessMsg>(ISuccessMsg.class, cmd2ModelAdpt.getLocalMember(), new SuccessMsg());
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				MsgAlgo.this.cmd2ModelAdpt = cmd2ModelAdpt;
			}});


		/**
		 * 	Ship location message
		 */
		this.setCmd(ShipLocMsg.class, new ADataPacketAlgoCmd<ADataPacket, ShipLocMsg, Void>(){

			/**
			 * Serial number
			 */
			private static final long serialVersionUID = -1736316193676303797L;

			@Override
			public ADataPacket apply(Class<?> index, DataPacket<ShipLocMsg> host,
					Void... params) {
				double curLat = host.getData().getCurLat();
				double curLon = host.getData().getCurLon();
				double desLat = host.getData().getDesLat();
				double desLon = host.getData().getDesLon();

				//update player ship position
				try {

					System.out.println(host.getSender().getName());
					System.out.println("Ship Current Lattitude is "+ curLat +". Longitude is " + curLon);
					System.out.println("Ship Destination Lattitude is "+ desLat +". Longitude is " + desLon);
					System.out.println("Moved!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
	
				} catch (RemoteException e) {

					e.printStackTrace();
				}

				return new DataPacket<ISuccessMsg>(ISuccessMsg.class, cmd2ModelAdpt.getLocalMember(), new SuccessMsg());
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				MsgAlgo.this.cmd2ModelAdpt = cmd2ModelAdpt;
			}

		});
		
		
		
		/**
		 * 	StopGameMsg message
		 */
		this.setCmd(StopGameMsg.class, new ADataPacketAlgoCmd<ADataPacket, StopGameMsg, Void>(){


			/**
			 * Serial number
			 */
			private static final long serialVersionUID = -1409797170157980610L;

			@Override
			public ADataPacket apply(Class<?> index, DataPacket<StopGameMsg> host,
					Void... params) {
				return new DataPacket<ISuccessMsg>(ISuccessMsg.class, cmd2ModelAdpt.getLocalMember(), new SuccessMsg());

			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				MsgAlgo.this.cmd2ModelAdpt = cmd2ModelAdpt;
			}

		});

		
		
		/**
		 * 	Ship location message
		 */
		this.setCmd(ShipLocMsg.class, new ADataPacketAlgoCmd<ADataPacket, ShipLocMsg, Void>(){

			/**
			 * Serial number
			 */
			private static final long serialVersionUID = -1736316193676303797L;

			@Override
			public ADataPacket apply(Class<?> index, DataPacket<ShipLocMsg> host,
					Void... params) {
				double curLat = host.getData().getCurLat();
				double curLon = host.getData().getCurLon();
				double desLat = host.getData().getDesLat();
				double desLon = host.getData().getDesLon();

				//update player ship position
				try {

					System.out.println(host.getSender().getName());
					System.out.println("Ship Current Lattitude is "+ curLat +". Longitude is " + curLon);
					System.out.println("Ship Destination Lattitude is "+ desLat +". Longitude is " + desLon);

					System.out.println("Moved!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
	
				} catch (RemoteException e) {

					e.printStackTrace();
				}

				return new DataPacket<ISuccessMsg>(ISuccessMsg.class, cmd2ModelAdpt.getLocalMember(), new SuccessMsg());
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				MsgAlgo.this.cmd2ModelAdpt = cmd2ModelAdpt;
			}

		});
		
		
		
		/**
		 * 	RejectMsg message
		 */
		this.setCmd(IRejectMsg.class, new ADataPacketAlgoCmd<ADataPacket, IRejectMsg, Void>(){

			/**
			 * Serial number
			 */
			private static final long serialVersionUID = 27254835451691918L;


			@Override
			public ADataPacket apply(Class<?> index,
					DataPacket<IRejectMsg> host, Void... params) {
				
				return new DataPacket<ISuccessMsg>(ISuccessMsg.class, cmd2ModelAdpt.getLocalMember(), new SuccessMsg());

			}


			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				MsgAlgo.this.cmd2ModelAdpt = cmd2ModelAdpt;
			}

			

		});

	}

}

