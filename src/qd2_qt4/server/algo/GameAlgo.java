package qd2_qt4.server.algo;

import java.rmi.RemoteException;
import java.util.UUID;

import common.IAddCmd;
import common.IChatroom;
import common.ICmd2ModelAdapter;
import common.IMember;
import common.ISuccessMsg;
import provided.datapacket.ADataPacket;
import provided.datapacket.ADataPacketAlgoCmd;
import provided.datapacket.DataPacket;
import provided.datapacket.DataPacketAlgo;
import provided.mixedData.MixedDataKey;
import qd2_qt4.game.controller.GameController;
import qd2_qt4.game.model.GameModel;
import qd2_qt4.server.message.AddCmd;
import qd2_qt4.server.message.CollectMsg;
import qd2_qt4.server.message.LocMsg;
import qd2_qt4.server.message.ShipLocMsg;
import qd2_qt4.server.message.ShipMsg;
import qd2_qt4.server.message.StartGameMsg;
import qd2_qt4.server.message.StopGameMsg;
import qd2_qt4.server.message.SuccessMsg;
import qd2_qt4.server.message.UpdateMsg;

/**
 * Algorithm to use for game.
 * @author qt4
 *
 */
public class GameAlgo extends DataPacketAlgo<ADataPacket, Void> {

	/**
	 * Serial number
	 */
	private static final long serialVersionUID = 3346955668065046937L;

	/**
	 * Command to model adapter.
	 */
	private transient ICmd2ModelAdapter cmd2ModelAdpt;

//	private IChatroom team;

	/**
	 * UUID
	 */
	private UUID uuid = UUID.fromString("38400001-8cf0-11bd-b23e-10b96e4ef00d");

	/**
	 * Constructor
	 * @param defaultCmd Default command
	 * @param cmd2MAdpt Command to model adapter.
	 * @param team Team.
	 */
	public GameAlgo(ADataPacketAlgoCmd<ADataPacket, Object, Void> defaultCmd, 
			ICmd2ModelAdapter cmd2MAdpt, IChatroom team) {
		super(defaultCmd);

		this.cmd2ModelAdpt = cmd2MAdpt;

		// StartGameMsg
		this.setCmd(StartGameMsg.class, new ADataPacketAlgoCmd<ADataPacket, StartGameMsg, Void>(){

			/**
			 * 
			 */
			private static final long serialVersionUID = 665365629345268244L;

			@Override
			public ADataPacket apply(Class<?> index, DataPacket<StartGameMsg> host,
					Void... params) {
				try {
					System.out.println(cmd2ModelAdpt.getLocalMember().getName() + " received StartGameMsg from: " + host.getSender().getName());
					//					System.out.println("Cmd ID is: " + host.getData().getID());
					System.out.println("Cmd2ModelAdpater is: " + cmd2ModelAdpt);
				} catch (RemoteException e1) {
					e1.printStackTrace();
				}

				IMember localMember = cmd2ModelAdpt.getLocalMember();
				// add location message algo

				try {
					@SuppressWarnings("unchecked")
					ADataPacketAlgoCmd<ADataPacket, ?, Void> cmd = (ADataPacketAlgoCmd<ADataPacket, ?, Void>) (new GameAlgo(null, cmd2ModelAdpt, team).getCmd(LocMsg.class));
					localMember.recvMessage(new DataPacket<IAddCmd>(IAddCmd.class, host.getSender(), new AddCmd(LocMsg.class, cmd)));

					@SuppressWarnings("unchecked")
					ADataPacketAlgoCmd<ADataPacket, ?, Void> collectCmd = (ADataPacketAlgoCmd<ADataPacket, ?, Void>) (new GameAlgo(null, cmd2ModelAdpt, team).getCmd(CollectMsg.class));
					localMember.recvMessage(new DataPacket<IAddCmd>(IAddCmd.class, host.getSender(), new AddCmd(CollectMsg.class, collectCmd)));
					
					@SuppressWarnings("unchecked")
					ADataPacketAlgoCmd<ADataPacket, ?, Void> shipCmd = (ADataPacketAlgoCmd<ADataPacket, ?, Void>) (new GameAlgo(null, cmd2ModelAdpt, team).getCmd(ShipMsg.class));
					localMember.recvMessage(new DataPacket<IAddCmd>(IAddCmd.class, host.getSender(), new AddCmd(ShipMsg.class, shipCmd)));

					//UpdateMsg
					@SuppressWarnings("unchecked")
					ADataPacketAlgoCmd<ADataPacket, ?, Void> updateCmd = (ADataPacketAlgoCmd<ADataPacket, ?, Void>) (new GameAlgo(null, cmd2ModelAdpt, team).getCmd(UpdateMsg.class));
					localMember.recvMessage(new DataPacket<IAddCmd>(IAddCmd.class, host.getSender(), new AddCmd(UpdateMsg.class, updateCmd)));

					//ShipLocMsg
					@SuppressWarnings("unchecked")
					ADataPacketAlgoCmd<ADataPacket, ?, Void> shipLocCmd = (ADataPacketAlgoCmd<ADataPacket, ?, Void>) (new GameAlgo(null, cmd2ModelAdpt, team).getCmd(ShipLocMsg.class));
					localMember.recvMessage(new DataPacket<IAddCmd>(IAddCmd.class, host.getSender(), new AddCmd(ShipLocMsg.class, shipLocCmd)));

					//StopGameMsg
					@SuppressWarnings("unchecked")
					ADataPacketAlgoCmd<ADataPacket, ?, Void> stopGameCmd = (ADataPacketAlgoCmd<ADataPacket, ?, Void>) (new GameAlgo(null, cmd2ModelAdpt, team).getCmd(StopGameMsg.class));
					localMember.recvMessage(new DataPacket<IAddCmd>(IAddCmd.class, host.getSender(), new AddCmd(StopGameMsg.class, stopGameCmd)));

					
				} catch (RemoteException e) {
					e.printStackTrace();
				}

				System.out.println("Adding locMsg");


				GameController game = new GameController(team);
				game.setMemberStub(localMember);
				game.setAllTeam(host.getData().getAllTeam());
				game.setTeamIdx(host.getData().getTeamIdx());
				//				game.setInitAlarms(alarms);
				game.setInitAlarms(host.getData().getInitAlarms());
				game.start();




				// add game model to MDD
				System.out.println(game.getGame());
				MixedDataKey<GameModel> key = new MixedDataKey<GameModel>(uuid, "Game Model", GameModel.class);
				cmd2ModelAdpt.getMDD().put(key, game.getGame());
				System.out.println("Key UUID is: " + uuid);
				System.out.println("Key hashcode is: " + key.hashCode());
				System.out.println(cmd2ModelAdpt.getMDD());

				System.out.println("uuid hash: "+ uuid.hashCode());
				System.out.println("Game Model hash: "+ "Game Model".hashCode());
				System.out.println("GameModel.class hash: "+ GameModel.class.hashCode());

				//				game.start();
				return new DataPacket<ISuccessMsg>(ISuccessMsg.class, localMember, new SuccessMsg());
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				GameAlgo.this.cmd2ModelAdpt = cmd2ModelAdpt;
			}



		});



		/**
		 * 	location message
		 */
		this.setCmd(LocMsg.class, new ADataPacketAlgoCmd<ADataPacket, LocMsg, Void>(){

			/**
			 * 
			 */
			private static final long serialVersionUID = -2090275446204592328L;

			@Override
			public ADataPacket apply(Class<?> index, DataPacket<LocMsg> host,
					Void... params) {
				double curLat = host.getData().getCurLat();
				double curLon = host.getData().getCurLon();
				double desLat = host.getData().getDesLat();
				double desLon = host.getData().getDesLon();
				boolean onBoard = host.getData().getOnBoard();

				//update player position
				try {
					MixedDataKey<GameModel> key = new MixedDataKey<GameModel>(uuid, "Game Model", GameModel.class);
					System.out.println(cmd2ModelAdpt.getLocalMember().getName() + " received LocMsg from: " + host.getSender().getName());
					//					System.out.println("Cmd ID is: " + host.getData().getID());
					System.out.println("Cmd2ModelAdpater is: " + cmd2ModelAdpt);
					System.out.println("key uuid is: " + uuid);
					System.out.println("uuid hash: "+ uuid.hashCode());
					System.out.println("Game Model hash: "+ "Game Model".hashCode());
					System.out.println("GameModel.class hash: "+ GameModel.class.hashCode());
					System.out.println(cmd2ModelAdpt.getMDD().get(key));


					GameModel game = cmd2ModelAdpt.getMDD().get(key);

					while (game==null){
						Thread.sleep(200);
						game = cmd2ModelAdpt.getMDD().get(key);
						System.out.println("Game model got: "+ game);
						System.out.println("MMD got: "+ cmd2ModelAdpt.getMDD());
						System.out.println("MMD contains key: "+ cmd2ModelAdpt.getMDD().containsKey(key));
						System.out.println("key hash is: "+ key.hashCode());

						System.out.println("uuid hash: "+ uuid.hashCode());
						System.out.println("Game Model hash: "+ "Game Model".hashCode());
						System.out.println("GameModel.class hash: "+ GameModel.class.hashCode());


					};

					System.out.println(host.getSender().getName());
					System.out.println("Current Lattitude is "+ curLat +". Longitude is " + curLon);
					System.out.println("Destination Lattitude is "+ desLat +". Longitude is " + desLon);

					
					game.movePlayer(host.getSender().getUUID(), curLat, curLon, desLat, desLon, onBoard);
					System.out.println("Moved!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

				} catch (RemoteException | InterruptedException e) {

					e.printStackTrace();
				}

				return new DataPacket<ISuccessMsg>(ISuccessMsg.class, cmd2ModelAdpt.getLocalMember(), new SuccessMsg());
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				GameAlgo.this.cmd2ModelAdpt = cmd2ModelAdpt;
			}

		});


		/**
		 * 	Collect message
		 */
		this.setCmd(CollectMsg.class, new ADataPacketAlgoCmd<ADataPacket, CollectMsg, Void>(){

			/**
			 * 
			 */
			private static final long serialVersionUID = 7440923604962910485L;

			@Override
			public ADataPacket apply(Class<?> index,
					DataPacket<CollectMsg> host, Void... params) {

				try {
					MixedDataKey<GameModel> key = new MixedDataKey<GameModel>(uuid, "Game Model", GameModel.class);

					GameModel game = cmd2ModelAdpt.getMDD().get(key);

					while (game==null){
						Thread.sleep(200);

						game = cmd2ModelAdpt.getMDD().get(key);
						System.out.println("Game model got: "+ game);
						System.out.println("MMD got: "+ cmd2ModelAdpt.getMDD());
						System.out.println("MMD contains key: "+ cmd2ModelAdpt.getMDD().containsKey(key));
						System.out.println("key hash is: "+ key.hashCode());

						System.out.println("uuid hash: "+ uuid.hashCode());
						System.out.println("Game Model hash: "+ "Game Model".hashCode());
						System.out.println("GameModel.class hash: "+ GameModel.class.hashCode());
					}

					game.removeAlarm(host.getData().getIcon());

				} catch (InterruptedException e) {
					e.printStackTrace();
				};


				return new DataPacket<ISuccessMsg>(ISuccessMsg.class, cmd2ModelAdpt.getLocalMember(), new SuccessMsg());
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				GameAlgo.this.cmd2ModelAdpt = cmd2ModelAdpt;
			}

		});


		/**
		 * 	Ship message
		 */
		this.setCmd(ShipMsg.class, new ADataPacketAlgoCmd<ADataPacket, ShipMsg, Void>(){

			/**
			 * 
			 */
			private static final long serialVersionUID = 7440923604962910485L;

			@Override
			public ADataPacket apply(Class<?> index,
					DataPacket<ShipMsg> host, Void... params) {

				try {
					System.out.println(cmd2ModelAdpt.getLocalMember().getName() + " received ShipMsg from: " + host.getSender().getName());

					MixedDataKey<GameModel> key = new MixedDataKey<GameModel>(uuid, "Game Model", GameModel.class);

					GameModel game = cmd2ModelAdpt.getMDD().get(key);
					
					while (game==null){
						Thread.sleep(200);

						game = cmd2ModelAdpt.getMDD().get(key);
						System.out.println("Game model got: "+ game);
						System.out.println("MMD got: "+ cmd2ModelAdpt.getMDD());
						System.out.println("MMD contains key: "+ cmd2ModelAdpt.getMDD().containsKey(key));
						System.out.println("key hash is: "+ key.hashCode());

						System.out.println("uuid hash: "+ uuid.hashCode());
						System.out.println("Game Model hash: "+ "Game Model".hashCode());
						System.out.println("GameModel.class hash: "+ GameModel.class.hashCode());
					}

					game.showShip(host.getData().getShipUUID());

				} catch (InterruptedException | RemoteException e) {
					e.printStackTrace();
				};


				return new DataPacket<ISuccessMsg>(ISuccessMsg.class, cmd2ModelAdpt.getLocalMember(), new SuccessMsg());
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				GameAlgo.this.cmd2ModelAdpt = cmd2ModelAdpt;
			}

		});
		
		
		
		/**
		 * 	Update score message
		 */
		this.setCmd(UpdateMsg.class, new ADataPacketAlgoCmd<ADataPacket, UpdateMsg, Void>(){

			/**
			 * 
			 */
			private static final long serialVersionUID = 6420260873374722558L;

			@Override
			public ADataPacket apply(Class<?> index,
					DataPacket<UpdateMsg> host, Void... params) {
				try {
					System.out.println(cmd2ModelAdpt.getLocalMember().getName() + " received UpdateMsg from: " + host.getSender().getName());

					MixedDataKey<GameModel> key = new MixedDataKey<GameModel>(uuid, "Game Model", GameModel.class);

					GameModel game = cmd2ModelAdpt.getMDD().get(key);
					
					while (game==null){
						Thread.sleep(200);

						game = cmd2ModelAdpt.getMDD().get(key);
						System.out.println("Game model got: "+ game);
						System.out.println("MMD got: "+ cmd2ModelAdpt.getMDD());
						System.out.println("MMD contains key: "+ cmd2ModelAdpt.getMDD().containsKey(key));
						System.out.println("key hash is: "+ key.hashCode());

						System.out.println("uuid hash: "+ uuid.hashCode());
						System.out.println("Game Model hash: "+ "Game Model".hashCode());
						System.out.println("GameModel.class hash: "+ GameModel.class.hashCode());
					}

					System.out.println("ScoreList: "+host.getData().getScoreList());
					game.updateScores(host.getData().getScoreList());
					game.showMsg(host.getData().getHint());

				} catch (InterruptedException | RemoteException e) {
					e.printStackTrace();
				};


				return new DataPacket<ISuccessMsg>(ISuccessMsg.class, cmd2ModelAdpt.getLocalMember(), new SuccessMsg());
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				GameAlgo.this.cmd2ModelAdpt = cmd2ModelAdpt;
				
			}
			
		});
		
		
		/**
		 * 	Ship location message
		 */
		this.setCmd(ShipLocMsg.class, new ADataPacketAlgoCmd<ADataPacket, ShipLocMsg, Void>(){
			/**
			 * 
			 */
			private static final long serialVersionUID = -6563444124535329945L;

			@Override
			public ADataPacket apply(Class<?> index, DataPacket<ShipLocMsg> host,
					Void... params) {
				double curLat = host.getData().getCurLat();
				double curLon = host.getData().getCurLon();
				double desLat = host.getData().getDesLat();
				double desLon = host.getData().getDesLon();
				//update player ship position
				try {
					MixedDataKey<GameModel> key = new MixedDataKey<GameModel>(uuid, "Game Model", GameModel.class);
					System.out.println(cmd2ModelAdpt.getLocalMember().getName() + " received ShipLocMsg from: " + host.getSender().getName());
					//					System.out.println("Cmd ID is: " + host.getData().getID());
					System.out.println("Cmd2ModelAdpater is: " + cmd2ModelAdpt);
					System.out.println("key uuid is: " + uuid);
					System.out.println("uuid hash: "+ uuid.hashCode());
					System.out.println("Game Model hash: "+ "Game Model".hashCode());
					System.out.println("GameModel.class hash: "+ GameModel.class.hashCode());
					System.out.println(cmd2ModelAdpt.getMDD().get(key));


					GameModel game = cmd2ModelAdpt.getMDD().get(key);

					while (game==null){
						Thread.sleep(200);
						game = cmd2ModelAdpt.getMDD().get(key);
						System.out.println("Game model got: "+ game);
						System.out.println("MMD got: "+ cmd2ModelAdpt.getMDD());
						System.out.println("MMD contains key: "+ cmd2ModelAdpt.getMDD().containsKey(key));
						System.out.println("key hash is: "+ key.hashCode());

						System.out.println("uuid hash: "+ uuid.hashCode());
						System.out.println("Game Model hash: "+ "Game Model".hashCode());
						System.out.println("GameModel.class hash: "+ GameModel.class.hashCode());


					};

					System.out.println(host.getSender().getName());
					System.out.println("Ship Current Lattitude is "+ curLat +". Longitude is " + curLon);
					System.out.println("Ship Destination Lattitude is "+ desLat +". Longitude is " + desLon);

					game.moveShip(host.getSender().getUUID(), curLat, curLon, desLat, desLon);
					System.out.println("Moved!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

				} catch (RemoteException | InterruptedException e) {

					e.printStackTrace();
				}

				return new DataPacket<ISuccessMsg>(ISuccessMsg.class, cmd2ModelAdpt.getLocalMember(), new SuccessMsg());
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				GameAlgo.this.cmd2ModelAdpt = cmd2ModelAdpt;
			}

		});
		
		
		
		/**
		 * 	StopGameMsg message
		 */
		this.setCmd(StopGameMsg.class, new ADataPacketAlgoCmd<ADataPacket, StopGameMsg, Void>(){


			/**
			 * 
			 */
			private static final long serialVersionUID = -1409797170157980610L;

			@Override
			public ADataPacket apply(Class<?> index, DataPacket<StopGameMsg> host,
					Void... params) {

				try {
					MixedDataKey<GameModel> key = new MixedDataKey<GameModel>(uuid, "Game Model", GameModel.class);
					
					System.out.println(cmd2ModelAdpt.getMDD().get(key));


					GameModel game = cmd2ModelAdpt.getMDD().get(key);

					while (game==null){
						Thread.sleep(200);
						game = cmd2ModelAdpt.getMDD().get(key);
						System.out.println("Game model got: "+ game);
						System.out.println("MMD got: "+ cmd2ModelAdpt.getMDD());
						System.out.println("MMD contains key: "+ cmd2ModelAdpt.getMDD().containsKey(key));
						System.out.println("key hash is: "+ key.hashCode());

						System.out.println("uuid hash: "+ uuid.hashCode());
						System.out.println("Game Model hash: "+ "Game Model".hashCode());
						System.out.println("GameModel.class hash: "+ GameModel.class.hashCode());


					};

					game.stopGame(host.getData().getMsg());

				} catch (InterruptedException e) {

					e.printStackTrace();
				}
				
				return new DataPacket<ISuccessMsg>(ISuccessMsg.class, cmd2ModelAdpt.getLocalMember(), new SuccessMsg());

			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				GameAlgo.this.cmd2ModelAdpt = cmd2ModelAdpt;
			}

		});

	}

}
