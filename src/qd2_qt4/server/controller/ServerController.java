package qd2_qt4.server.controller;

import java.util.Collection;
import java.util.Vector;

import qd2_qt4.server.model.ServerModel;
import qd2_qt4.server.model.IViewAdapter;
import qd2_qt4.server.view.ServerGUI;
import qd2_qt4.server.view.IModelAdapter;
import common.IPerson;

/**
 * The controller of the game server.
 * @author qd2, qt4
 *
 */
public class ServerController {
	/**
	 * Model of the MVC.
	 */
	private ServerModel model;
	/**
	 * View of the MVC
	 */
	private ServerGUI view;
	
	/**
	 * Constructor of the controller.
	 */
	public ServerController() {
		model = new ServerModel(new IViewAdapter() {

			@Override
			public void append(String s) {
				view.append(s);
				
			}
			/**
			 * choice window for user to make choices
			 */
			@Override
			public String choiceWindow(String[] choices, String message,
					String title) {
				return view.choiceWindow(choices, message, title);
			}
			/**
			 * input window to get the input of user
			 */
			@Override
			public String inputWindow(String message, String defaultValue) {
				return view.inputWindow(message, defaultValue);
			}
			/**
			 * yes or no window for user 
			 */
			@Override
			public int yesNoWindow(String message, String title) {
				return view.yesNoWindow(message, title);
			}
			
			/**
			 * Update the user list on the view. 
			 */
			@Override
			public void updateConnectedList() {
				view.updateConnectedList();
				
			}
			@Override
			public void updateTeamList() {
				view.updateTeamList();
				
			}
			
		});
		/**
		 * Make the view. 
		 */
		view = new ServerGUI(new IModelAdapter() {
			/**
			 * Connect to the given ip or hostname. 
			 */
			@Override
			public String connectTo(String s) {
				return model.connectTo(s);
			}
			/**
			 * Get connected users. 
			 */
			@Override
			public Collection<IPerson> getConnection() {
				return model.getConnection();
			}

			@Override
			public void makeChatroom(String name) {
				model.makeChatroom(name);
				
			}
		
			@Override
			public void startGame(int selectedTeam) {
				model.startGame(selectedTeam);
				
			}
			
			@Override
			public Vector<String> getTeam() {
				return model.getChatroom();
			}
			@Override
			public void invite(int[] selectedPlayer, int team) {
				model.invite(selectedPlayer, team);
				
			}
			@Override
			public Vector<String> getMembers(int selectedChatroom) {
				return model.getMembers(selectedChatroom);
			}
			@Override
			public void stopGame() {
				model.stopGame();
				
			}
			@Override
			public void quit() {
				model.stop();
				
			}
			
		});
	}
	
	/**
	 * Start main model and view.
	 */
	public void start() {
		model.start();
		view.start();
	}
	
	/**
	 * Start the controller and the application.
	 * @param args Arguments if applicable.
	 */
	public static void main(String[] args) {
		(new ServerController()).start();
	}
}
