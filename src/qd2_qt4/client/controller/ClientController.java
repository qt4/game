package qd2_qt4.client.controller;

import java.awt.Component;
import java.rmi.RemoteException;
import java.util.Collection;

import javax.swing.SwingUtilities;

import provided.datapacket.ADataPacket;
import qd2_qt4.client.model.ClientModel;
import qd2_qt4.client.model.IClientViewAdapter;
import qd2_qt4.client.model.IMiniViewAdapter;
import qd2_qt4.client.model.MiniModel;
import qd2_qt4.client.view.ClientGUI;
import qd2_qt4.client.view.IClientModelAdapter;
import qd2_qt4.client.view.IMiniModelAdapter;
import qd2_qt4.client.view.MiniGUI;

/**
 * Controller of client MVC
 * @author qt4
 *
 */
public class ClientController {
	/**
	 * Model of the MVC.
	 */
	private ClientModel model;
	/**
	 * View of the MVC
	 */
	private ClientGUI view;
	
	/**
	 * Constructor of the controller.
	 */
	public ClientController() {
		model = new ClientModel(new IClientViewAdapter(){

			@Override
			public void append(String s) {
				view.append(s);
				
			}

			/**
			 * input window to get the input of user
			 */
			@Override
			public String inputWindow(String message, String defaultValue) {
				return view.inputWindow(message, defaultValue);
			}
			/**
			 * yes or no window for user 
			 */
			@Override
			public int yesNoWindow(String message, String title) {
				return view.yesNoWindow(message, title);
			}
			/**
			 * Make a mini view adapter. 
			 */
			@Override
			public IMiniViewAdapter makeMiniViewAdapter(MiniModel miniModel) {
				IMiniModelAdapter miniModelAdapter = new IMiniModelAdapter() {

					@Override
					public void sendMsg(String text) {
						try {
							miniModel.sendMsg(text);
						} catch (RemoteException e) {
							e.printStackTrace();
						}
						
					}

					@Override
					public Collection<ADataPacket> leave() {
						return miniModel.leave();
					}

					@Override
					public String getMemberList() {
						return miniModel.getMemberList();
					}
					
				};				
				
				//make mini MVC
				MiniGUI miniGUI = view.makeMiniGUI(miniModelAdapter);
				miniGUI.start();

				IMiniViewAdapter miniViewAdapter = new IMiniViewAdapter() {

					@Override
					public void append(String s) {
						miniGUI.append(s);
						
					}

					@Override
					public void updateList(String members) {
						miniGUI.updateList(members);
					}

					@Override
					public void setTitle(String name) {
						miniGUI.setTitle(name);
						
					}

					@Override
					public void displayCompMsg(String name, Component comp) {
						miniGUI.displayCompMsg(name, comp);
						
					}

					@Override
					public void updateList() {
						miniGUI.updateList();
						
					}


				};	
				
				return miniViewAdapter;


			}
	
						
		});
		/**
		 * Make the view. 
		 */
		view = new ClientGUI(new IClientModelAdapter() {
			/**
			 * Connect to the given ip or hostname. 
			 */
			@Override
			public String connectTo(String s) {
				return model.connectTo(s);
			}
			
		});
	}
	
	/**
	 * Start main model and view.
	 */
	public void start() {
		model.start();
		view.start();
	}
	
	/**
	 * Start the controller and the application.
	 * @param args Arguments if applicable.
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				(new ClientController()).start();
			}
		});
		
	}
}
