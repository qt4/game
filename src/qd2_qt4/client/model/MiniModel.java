package qd2_qt4.client.model;

import java.awt.Component;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Collection;
import java.util.UUID;

import javax.swing.JFrame;

import qd2_qt4.client.message.JoinRoomMsg;
import qd2_qt4.client.message.LeaveRoomMsg;
import qd2_qt4.client.message.TextMsg;
import provided.datapacket.ADataPacket;
import provided.datapacket.DataPacket;
import provided.mixedData.IMixedDataDictionary;
import provided.mixedData.MixedDataDictionary;
import provided.mixedData.MixedDataKey;
import common.ICmd2ModelAdapter;
import common.IChatroom;
import common.IJoinRoomMsg;
import common.ILeaveRoomMsg;
import common.IMember;
import common.ITextMsg;

/**
 * Mini model of client.
 * @author qt4
 *
 */
/**
 * @author Capo
 *
 */
public class MiniModel {
	/**
	 * mini model to view adapter
	 */
	private transient IMiniViewAdapter miniView;
	
	/**
	 * Member 
	 */
	private ClientMember myMember;
	
	/**
	 * Member stub
	 */
	private IMember myMemberStub;;
	
	/**
	 * UUID of member stub.
	 */
	private UUID myUUID;
	/**
	 * Chatroom
	 */
	private IChatroom chatroom;
	
	/**
	 * Mixed data dictionary. 
	 */
	private IMixedDataDictionary dataDict = new MixedDataDictionary();
	/**
	 * Constructor
	 * @param myUUID UUID of person.
	 * @param chatroom  Team.
	 */
	
	public MiniModel(UUID myUUID, IChatroom chatroom){
		this.myUUID = myUUID;
		myMember = new ClientMember(myUUID, chatroom, cmd2ModelAdapter);
		System.out.println("Instantiated a new IMember");
		myMemberStub = null;
		try {
			myMemberStub = (IMember) UnicastRemoteObject.exportObject(myMember, 2003);			
		} catch (RemoteException e) {
			e.printStackTrace();
		}
			
	}
	


//	/**
//	 * Set the stub of the chatroom
//	 * @param selfuser Member
//	 */
//	public void setMyMember(ClientMember myMember) {
//		this.myMember = myMember; 
//	}
	
//	public void setMyMemberStub(IMember myMemberStub) {
//		this.myMemberStub = myMemberStub; 
//	}
	

	/**
	 * Set the adapter
	 * @param miniViewAdapter A mini model to view adapter.
	 */
	public void setAdapter(IMiniViewAdapter miniViewAdapter) {
		this.miniView=miniViewAdapter;
		dataDict.put(new MixedDataKey<IMiniViewAdapter>(myUUID, "miniView", IMiniViewAdapter.class), this.miniView);
	}
	
	
	/**
	 * Set the chatroom
	 * @param chatroom chatroom
	 */
	public void setChatroom (IChatroom chatroom){
		this.chatroom=chatroom;
		miniView.setTitle("Chatroom Name: "+chatroom.getName());
	}
	

	/**
	 * Command to model adapter
	 */	
	private transient ICmd2ModelAdapter cmd2ModelAdapter = new ICmd2ModelAdapter(){

		@Override
		public IMember getLocalMember() {
			return myMemberStub;
		}

		@Override
		public void displayStringMsg(String s) {
			miniView.append(s);
		}

		@Override
		public void displayComponentMsg(String name, Component comp) {
			miniView.displayCompMsg(name, comp);
		}

		@Override
		public void addGameComponent(String name, Component comp) {
			System.out.println("Before add a component");
			JFrame frame = new JFrame();
			frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			frame.add(comp);
			System.out.println("Added a new Game Component.");
			frame.pack();
			frame.setVisible(true);
		}

		@Override
		public void send2Member(IMember member, ADataPacket msg) {
			try {
				member.recvMessage(msg);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
			
		}

		@Override
		public void send2Team(ADataPacket msg) {
			chatroom.broadcastMsg(msg);
		}

		@Override
		public IMixedDataDictionary getMDD() {
			return dataDict;
		}
	};

	
	
	
	/**
	 * Send text message
	 * @param text Message to send
	 * @throws RemoteException remote exception
	 */
	public void sendMsg(String text) throws RemoteException {
		if (!text.equals("")) {
			chatroom.broadcastMsg(new DataPacket<ITextMsg>(ITextMsg.class, this.myMember, new TextMsg(text)));			
		}
	}


	/**
	 * Leave chatroom
	 * @return retrun message.
	 */
	public Collection<ADataPacket> leave() {
		Collection<ADataPacket> res =chatroom.broadcastMsg(new DataPacket<ILeaveRoomMsg>(ILeaveRoomMsg.class, this.myMember, new LeaveRoomMsg(this.myMember)));
		return res;	
	}


	/**
	 * Get member list in a chatroom
	 * @return Member list
	 */
	public String getMemberList() {
		Collection<IMember> memberList = chatroom.getMembers();
		StringBuilder s = new StringBuilder();
		for(IMember member : memberList ){
			try {
				s.append(member.getName());
				s.append(System.getProperty("line.separator"));
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
		return s.toString();
	}

	/**
	 * Get a chatroom
	 * @return chatroom
	 */
	public IChatroom getChatroom() {
		return chatroom;
	}
	
	
	/**
	 * Join a chatroom
	 */
	public void join() {
		chatroom.broadcastMsg(new DataPacket<IJoinRoomMsg>(IJoinRoomMsg.class, myMemberStub, new JoinRoomMsg(myMemberStub)));
		chatroom.addMember(myMemberStub);
		miniView.setTitle(chatroom.getName());
		try {
			chatroom.broadcastMsg(new DataPacket<ITextMsg>(ITextMsg.class, myMemberStub, new TextMsg(myMemberStub.getName()+" joined in the Chatroom.")));
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		miniView.updateList();
	}
}
