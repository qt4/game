package qd2_qt4.client.model;

import qd2_qt4.client.model.IMiniViewAdapter;
import qd2_qt4.client.model.MiniModel;

/**
 * Client main model to view adapter.
 * @author qt4
 *
 */
public interface IClientViewAdapter {

	/**
	 * Append text message to the view.
	 * @param s Text message to show.
	 */
	void append(String s);

	/**
	 * Input window to use.
	 * @param string Tip message.
	 * @param defaultValue Defaul value for the window.
	 * @return Return message of the window.
	 */
	String inputWindow(String string, String defaultValue);

	/**
	 * Message window to use.
	 * @param message Tip message.
	 * @param title Title of the window.
	 * @return Yes or No option.
	 */
	int yesNoWindow(String message, String title);
	
	/**
	 * Make a mini model to view adapter
	 * @param miniModel A mini model.
	 * @return A mini model to view Adapter.
	 */
	public IMiniViewAdapter makeMiniViewAdapter(MiniModel miniModel);

}
