package qd2_qt4.client.model;

import java.rmi.RemoteException;
import java.util.UUID;

import qd2_qt4.client.algo.ClientAlgo;
import qd2_qt4.client.message.FailMsg;
import qd2_qt4.client.message.RequestCmd;
import provided.datapacket.ADataPacket;
import provided.datapacket.ADataPacketAlgoCmd;
import provided.datapacket.DataPacket;
import provided.datapacket.DataPacketAlgo;
import provided.mixedData.MixedDataKey;
import qd2_qt4.client.message.SuccessMsg;
import common.IChatroom;
import common.ICmd2ModelAdapter;
import common.IFailMsg;
import common.IJoinRoomMsg;
import common.ILeaveRoomMsg;
import common.IMember;
import common.IRequestCmd;
import common.ISuccessMsg;

/**
 * Implementation of client member.
 * @author qt4
 *
 */
public class ClientMember implements IMember {
	/**
	 * Member name
	 */
	private transient String memberName = System.getProperty("user.name");

	/**
	 * Chatroom the member is in.
	 */
	private transient IChatroom chatroom;

	/**
	 * UUID of the member.
	 */
	private UUID uuid;

	/**
	 * Algorithm carried with this member.
	 */
	private transient DataPacketAlgo<ADataPacket, Void> algo;

	/**
	 * Command to model adapter.
	 */
	private transient ICmd2ModelAdapter cmd2ModelAdpt;

	/**
	 * Constructor of the member.
	 * @param uuid UUID of the member.
	 * @param chatroom Chatroom the member is in.
	 * @param cmd2ModelAdpt Command to model adapter.
	 */
	public ClientMember(UUID uuid, IChatroom chatroom, ICmd2ModelAdapter cmd2ModelAdpt){
		this.uuid = uuid;
		this.chatroom = chatroom;
		System.out.println("chatroom:"+chatroom);
		this.cmd2ModelAdpt = cmd2ModelAdpt;
		this.algo = newAlgo();
	}

	/**
	 * Set the comand to model adapter.
	 * @param cmd2ModelAdpt The command to model adapter to use.
	 */
	public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
		this.cmd2ModelAdpt = cmd2ModelAdpt;

	}

	/**
	 * Get the name of the member.
	 * @return The name of the member.
	 */
	@Override
	public String getName() throws RemoteException {
		return memberName;
	}

	/**
	 * Get the UUID of the member.
	 * @return The UUID of the member.
	 */
	@Override
	public UUID getUUID() throws RemoteException {
		return uuid;
	}

	/**
	 * Receive messages.
	 * @param msg Message that received.
	 */
	@Override
	public ADataPacket recvMessage(ADataPacket msg) throws RemoteException {
		return msg.execute(algo, new Void[0]);
//		return msg.execute(algo);
	}


	/**
	 * Make a instance of a new algorithm.
	 * @return A new algorithm.
	 */
	private DataPacketAlgo<ADataPacket, Void> newAlgo(){
		/**
		 * Default command
		 */
		DataPacketAlgo<ADataPacket, Void> newAlgo = new ClientAlgo(new ADataPacketAlgoCmd<ADataPacket, Object, Void>(){

			/**
			 * Serial number
			 */
			private static final long serialVersionUID = 1213071532179293924L;

			@Override
			public ADataPacket apply(Class<?> index, DataPacket<Object> host,
					Void... params) {
				try {
					System.out.println(cmd2ModelAdpt.getLocalMember().getName() + " received UnkownMsg from: " + host.getSender().getName());

					ADataPacket reply = host.getSender().recvMessage(new DataPacket<IRequestCmd>(IRequestCmd.class, ClientMember.this, (IRequestCmd)new RequestCmd(index)));
					reply.execute(algo, new Void[0]);
					@SuppressWarnings("unchecked")
					ADataPacketAlgoCmd<ADataPacket, ?, Void> cmd =  (ADataPacketAlgoCmd<ADataPacket, ?, Void>)algo.getCmd(index);
					System.out.println("Cmd is: "+ cmd);
					if (cmd!=null){
						cmd.setCmd2ModelAdpt(cmd2ModelAdpt);
					}
					return host.execute(algo, new Void[0]);
				} catch (RemoteException e) {
					e.printStackTrace();
				}
				return new DataPacket<IFailMsg>(IFailMsg.class, ClientMember.this, new FailMsg("Default Cmd Failed!"));
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				ClientMember.this.cmd2ModelAdpt = cmd2ModelAdpt;

			}}, cmd2ModelAdpt);


		/**
		 * Join message
		 */
		newAlgo.setCmd(IJoinRoomMsg.class, new ADataPacketAlgoCmd<ADataPacket, IJoinRoomMsg, Void>(){

			/**
			 * Serial number
			 */
			private static final long serialVersionUID = 1213071532179293924L;

			@Override
			public ADataPacket apply(Class<?> index,
					DataPacket<IJoinRoomMsg> host, Void... params) {
				try {
					System.out.println(cmd2ModelAdpt.getLocalMember().getName() + " received JoinMsg from: " + host.getSender().getName());
					System.out.println("New member name: "+ host.getSender().getName());
					chatroom.addMember(host.getSender());
					cmd2ModelAdpt.getMDD().get(new MixedDataKey<IMiniViewAdapter>(uuid, "miniView", IMiniViewAdapter.class)).updateList();
				} catch (RemoteException e) {
					e.printStackTrace();
				}
				return new DataPacket<ISuccessMsg>(ISuccessMsg.class, ClientMember.this, new SuccessMsg());
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				ClientMember.this.cmd2ModelAdpt = cmd2ModelAdpt;
			}

		});


		/**
		 * Leave room message
		 */
		newAlgo.setCmd(ILeaveRoomMsg.class, new ADataPacketAlgoCmd<ADataPacket, ILeaveRoomMsg, Void>(){


			/**
			 * Serial number
			 */
			private static final long serialVersionUID = 7494248407659974865L;

			@Override
			public ADataPacket apply(Class<?> index,
					DataPacket<ILeaveRoomMsg> host, Void... params) {
				try {
//					chatroom.removeMember(host.getData().getMember());
					chatroom.removeMember(host.getSender());
					cmd2ModelAdpt.getMDD().get(new MixedDataKey<IMiniViewAdapter>(uuid, "miniView", IMiniViewAdapter.class)).updateList();
				} catch (Exception e) {
					e.printStackTrace();
				}


				return new DataPacket<ISuccessMsg>(ISuccessMsg.class, ClientMember.this, new SuccessMsg());
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				ClientMember.this.cmd2ModelAdpt = cmd2ModelAdpt;
			}

		});


		return newAlgo;
	}

}

