package qd2_qt4.client.model;

import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import provided.rmiUtils.IRMIUtils;
import provided.rmiUtils.IRMI_Defs;
import provided.rmiUtils.RMIUtils;
import provided.util.IVoidLambda;
import common.IChatroom;
import common.IPerson;
import qd2_qt4.client.model.MiniModel;

/**
 * Main model of the client.
 * @author qt4
 *
 */
public class ClientModel {

	/**
	 * Client model to view adapter.
	 */
	private IClientViewAdapter view;
	/**
	 * Person object of the client.
	 */
	private IPerson myPerson;
	/**
	 * Name of a person stub.
	 */
	private String myPersonName;
	/**
	 * Person stub.
	 */
	@SuppressWarnings("unused")
	private IPerson localPersonStub;
//	private Registry registry;
	/**
	 * UUID of a IPerson.
	 */
	private UUID myUUID = UUID.randomUUID();
	/**
	 * Chatroom list that the client is currently in.
	 */
	private List<IChatroom> chatroomList = new ArrayList<IChatroom>();
	
	/**
	 * Connected user list.
	 */
	private ArrayList<IPerson> myConnection = new ArrayList<IPerson>();
	
	
	/**
	 * A command used as a wrapper around the view adapter for the IRMIUtils and the ComputeEngine.
	 */
	private IVoidLambda<String> outputCmd = new IVoidLambda<String> (){
		public void apply(String... strs){
			for(String s: strs)view.append(s);
		}
	};	
	
	/**
	 * RMI utilities.
	 */
	private IRMIUtils rmiUtils = new RMIUtils(outputCmd);

	

	/**
	 * Contructor of the client main model
	 * @param view A main model to view adapter.
	 */
	public ClientModel(IClientViewAdapter view) {
		this.view = view;
		
		/**
		 * Top level stub. 
		 */
		myPerson = new IPerson() {
			
		
			@Override
			public int connectBack(IPerson myPersonStub) throws RemoteException {
				Iterator<IPerson> iterator = myConnection.iterator();
				
				while (iterator.hasNext()) {
					IPerson p = iterator.next();
					try {
						if (p.getUUID().equals(myPersonStub.getUUID())) return STATUS_SUCC;
					} catch (Exception e) {
						e.printStackTrace();
						view.append("Exception caught when searching for IPerson stub");
						iterator.remove();
					}
				}
				
				myConnection.add(myPersonStub);
				view.append("Connection to " + myPersonStub.getName() + " established!\n");
				return STATUS_SUCC;
			}

			@Override
			public int acceptInvitation(IChatroom chatroom)
					throws RemoteException {

				MiniModel miniModel = new MiniModel(myUUID, chatroom);
				miniModel.setAdapter(view.makeMiniViewAdapter(miniModel));
				miniModel.setChatroom(chatroom);

				chatroomList.add(chatroom);
				System.out.println("member size: "+chatroom.getMembers().size());

				miniModel.join();
				view.append("Joined Chatroom " + chatroom.getName() + "\n");
				
				return IPerson.STATUS_SUCC;
			}

			@Override
			public int recvRequest(IPerson requesterStub)
					throws RemoteException {
				// reject request
				return 0;
			}

			@Override
			public String getName() throws RemoteException {
				return myPersonName;
			}

			@Override
			public UUID getUUID() throws RemoteException {
				return myUUID;
			}
		};
	}
	
	/**
	 * Start the model.
	 */
	public void start() {
		rmiUtils.startRMI(IRMI_Defs.CLASS_SERVER_PORT_CLIENT);
		
		try {
			String name = view.inputWindow("Input your name: ", System.getProperty("user.name"));
			this.myPersonName = name;
			localPersonStub = (IPerson) UnicastRemoteObject.exportObject(myPerson, IPerson.CLIENT_BOUND_PORT);
//			view.append("Looking for registry..."+"\n");
			
//			registry = rmiUtils.getLocalRegistry();
//			
//			view.append("Found registry: " + registry + "\n");
//			registry.rebind(IPerson.CLIENT_BOUND_NAME, localPersonStub);
//			view.append("IPerson bound to " + IPerson.CLIENT_BOUND_NAME + "\n");
		}
		catch(Exception e) {
			System.err.println("IPerson exception:" + "\n");
			e.printStackTrace();
			System.exit(-1);
		}
		
		view.append("Waiting..."+"\n");
	}

	/**
	 * Connect to a remote host or IP address.
	 * @param remoteHost Host name or IP.
	 * @return A result status string.
	 */
	public String connectTo(String remoteHost) {
		try {
			Thread thread = new Thread() {
				public void run() {
					try {
						view.append("Locating registry at " + remoteHost + "...\n");
						Registry registry = rmiUtils.getRemoteRegistry(remoteHost);
						view.append("Found registry: " + registry + "\n");
						IPerson remoteP = (IPerson) registry.lookup(IPerson.SERVER_BOUND_NAME);
						view.append("Found remote IPerson object: " + remoteP + "\n");
						myPerson.connectBack(remoteP);		
						if (remoteP.connectBack(myPerson)==IPerson.STATUS_SUCC) {
							if (remoteP.recvRequest(myPerson)!=IPerson.STATUS_SUCC)
								view.append("Request failed!");
						} else {
							view.append("Connect failed!");
						}
						
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
			};
			thread.start();
		} catch(Exception e) {
			view.append("Exception connecting to " + remoteHost + ": " + e + "\n");
			e.printStackTrace();
			return "No connection established!";
		}
		return "Connection to " + remoteHost + "  established!";
	}

}
