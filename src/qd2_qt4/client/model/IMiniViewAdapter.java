package qd2_qt4.client.model;

import java.awt.Component;

/**
 * Mini model to view adapter.
 * @author qt4
 *
 */
public interface IMiniViewAdapter {

	/**
	 * Add text
	 * @param s text
	 */
	void append(String s);

	/**
	 * Update the person list.
	 * @param members Member names
	 */
	void updateList(String members);

	/**
	 * Set the chatroom title
	 * @param name Title to set.
	 */
	void setTitle(String name);

	/**
	 * Display a component message.
	 * @param name	 the name of the component
	 * @param comp	the new component
	 */
	void displayCompMsg(String name, Component comp);

	/**
	 * Update the member list.
	 */
	void updateList();
	
}
