package qd2_qt4.client.algo;

import java.rmi.RemoteException;
import java.util.Date;

import common.ICmd2ModelAdapter;
import common.IFailMsg;
import common.IRejectMsg;
import common.IRequestCmd;
import common.ISuccessMsg;
import common.ITextMsg;
import common.IAddCmd;
import provided.datapacket.ADataPacket;
import provided.datapacket.ADataPacketAlgoCmd;
import provided.datapacket.DataPacket;
import provided.datapacket.DataPacketAlgo;
import provided.extvisitor.IExtVisitorCmd;
import qd2_qt4.client.message.AddCmd;
import qd2_qt4.client.message.SuccessMsg;

/**
 * Client message algorithm
 * @author qt4
 *
 */
public class ClientAlgo extends DataPacketAlgo<ADataPacket, Void> {

	/**
	 * serial number
	 */
	private static final long serialVersionUID = 762868564162394668L;
	/**
	 * Command to model adapter
	 */
	private transient ICmd2ModelAdapter cmd2ModelAdpt;


	/**
	 * Constructor
	 * @param defaultCmd Default command
	 * @param cmd2MAdpt Command to model adapter
	 */
	public ClientAlgo(ADataPacketAlgoCmd<ADataPacket, Object, Void> defaultCmd, ICmd2ModelAdapter cmd2MAdpt) {
		super(defaultCmd);
		
		this.cmd2ModelAdpt = cmd2MAdpt;

		/**
		 *  set TextMsg Command.
		 */
		this.setCmd(ITextMsg.class, new ADataPacketAlgoCmd<ADataPacket, ITextMsg, Void>(){

			/**
			 * 
			 */
			private static final long serialVersionUID = 4488472418196793722L;

			@Override
			public ADataPacket apply(Class<?> index, DataPacket<ITextMsg> host,
					Void... params) {
				String text = host.getData().getText();
				String senderName = " ";
				try {
					senderName = host.getSender().getName();
				} catch (RemoteException e) {
					e.printStackTrace();
				}
				System.out.println(text);
				System.out.println("cmd2ModelAdpt: "+ cmd2ModelAdpt);
				
				ClientAlgo.this.cmd2ModelAdpt.displayStringMsg(new Date().toString()+"\n"+senderName+" says: "+text);
				return new DataPacket<ISuccessMsg>(ISuccessMsg.class, cmd2ModelAdpt.getLocalMember(), new SuccessMsg());
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				ClientAlgo.this.cmd2ModelAdpt = cmd2ModelAdpt;
			}

		});

		
		
		/**
		 *  Set RequestCmd.
		 */
		this.setCmd(IRequestCmd.class, new ADataPacketAlgoCmd<ADataPacket, IRequestCmd, Void>(){

			/**
			 * 
			 */
			private static final long serialVersionUID = 2313399919455080516L;

			@SuppressWarnings("unchecked")
			@Override
			public ADataPacket apply(Class<?> index, DataPacket<IRequestCmd> host,
					Void... params) {
				
				Class<?> idx = host.getData().getID();
				try {
					System.out.println(cmd2ModelAdpt.getLocalMember().getName() + " received RequestCmd from: " + host.getSender().getName());
					System.out.println("Cmd ID is: " + idx);
					System.out.println("Cmd2ModelAdpater is: " + cmd2ModelAdpt);
					
				} catch (RemoteException e) {
					e.printStackTrace();
				}
				return new DataPacket<IAddCmd>(IAddCmd.class, cmd2ModelAdpt.getLocalMember(), new AddCmd(idx, (ADataPacketAlgoCmd<ADataPacket, ?, Void>) ClientAlgo.this.getCmd(idx)));

			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				ClientAlgo.this.cmd2ModelAdpt = cmd2ModelAdpt;
			}
			
		});
		
		/**
		 *  Set AddCmd.
		 */
		this.setCmd(IAddCmd.class, new ADataPacketAlgoCmd<ADataPacket, IAddCmd, Void>(){

			/**
			 * 
			 */
			private static final long serialVersionUID = -1655939950799464594L;

			@Override
			public ADataPacket apply(Class<?> index, DataPacket<IAddCmd> host,
					Void... params) {
				
				try {
					System.out.println(cmd2ModelAdpt.getLocalMember().getName() + " received AddCmd from: " + host.getSender().getName());
					System.out.println("Cmd ID is: " + host.getData().getID());
					System.out.println("Cmd2ModelAdpater is: " + cmd2ModelAdpt);
					
					ClientAlgo.this.setCmd(host.getData().getID(), (IExtVisitorCmd<ADataPacket, Class<?>, Void, ADataPacket>) host.getData().getCmd());
					
					if (host.getData().getCmd()!=null){
						host.getData().getCmd().setCmd2ModelAdpt(cmd2ModelAdpt);
					}
				} catch (RemoteException e) {
					cmd2ModelAdpt.displayStringMsg("Exception when adding Cmd.");
					e.printStackTrace();
				}
				
				return new DataPacket<ISuccessMsg>(ISuccessMsg.class, cmd2ModelAdpt.getLocalMember(), new SuccessMsg());
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				ClientAlgo.this.cmd2ModelAdpt = cmd2ModelAdpt;
			}

			
			
		});
		
		
		/**
		 *  success message
		 */
		this.setCmd(ISuccessMsg.class, new ADataPacketAlgoCmd<ADataPacket, ISuccessMsg, Void>(){

		
			/**
			 * 
			 */
			private static final long serialVersionUID = -448317610685305867L;

			@Override
			public ADataPacket apply(Class<?> index, DataPacket<ISuccessMsg> host,
					Void... params) {
				return null;
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				ClientAlgo.this.cmd2ModelAdpt = cmd2ModelAdpt;
			}

		});
		
		
		/**
		 * 	fail message
		 */
		this.setCmd(IFailMsg.class, new ADataPacketAlgoCmd<ADataPacket, IFailMsg, Void>(){

			
			/**
			 * 
			 */
			private static final long serialVersionUID = -6118775019987374522L;

			@Override
			public ADataPacket apply(Class<?> index, DataPacket<IFailMsg> host,
					Void... params) {
				ClientAlgo.this.cmd2ModelAdpt.displayStringMsg(host.getData().getErrorText());
				return null;
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				ClientAlgo.this.cmd2ModelAdpt = cmd2ModelAdpt;
			}

		});
		
		
		/**
		 * 	RejectMsg message
		 */
		this.setCmd(IRejectMsg.class, new ADataPacketAlgoCmd<ADataPacket, IRejectMsg, Void>(){

			/**
			 * 
			 */
			private static final long serialVersionUID = 7309584389745381789L;


			@Override
			public ADataPacket apply(Class<?> index,
					DataPacket<IRejectMsg> host, Void... params) {
				
				return new DataPacket<ISuccessMsg>(ISuccessMsg.class, cmd2ModelAdpt.getLocalMember(), new SuccessMsg());

			}


			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				ClientAlgo.this.cmd2ModelAdpt = cmd2ModelAdpt;
			}

		});
		
		
				
	}

	public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
		this.cmd2ModelAdpt = cmd2ModelAdpt;

	}

}

