package qd2_qt4.client.message;

import common.IRequestCmd;

/**
 * Known message, request command.
 * @author qt4
 *
 */
public class RequestCmd implements IRequestCmd{
	/**
	 * Serial number
	 */
	private static final long serialVersionUID = 4981096077135214175L;
	/**
	 * Class type as ID
	 */
	private Class<?> classtype;
	
	/**
	 * Constructor
	 * @param classtype As ID
	 */
	public RequestCmd(Class<?> classtype){
		this.classtype = classtype;
	}
	
	/**
	 * Get the command ID.
	 * @return Class type as command ID.
	 */
	@Override
	public Class<?> getID() {
		return classtype;
	}

}
