package qd2_qt4.client.message;

import common.ISuccessMsg;

/**
 * Known message, successful message.
 * @author qt4
 *
 */
public class SuccessMsg implements ISuccessMsg{

	/**
	 * Serial number
	 */
	private static final long serialVersionUID = -1987736909132475631L;

}
