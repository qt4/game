package qd2_qt4.client.message;

import common.ILeaveRoomMsg;
import common.IMember;

/**
 * Known message, leave room message.
 * @author qt4
 *
 */
public class LeaveRoomMsg implements ILeaveRoomMsg{

	/**
	 * Serial number
	 */
	private static final long serialVersionUID = 8871948310615801830L;
	/**
	 * The member stub who leaves the room.
	 */
	private IMember myMember;
	/**
	 * Constructor of the leave message.
	 * @param member The member stub who leaves the chat room.
	 */
	public LeaveRoomMsg(IMember member){
		this.myMember = member;
	}
	/**
	 * Get the member stub who leaves the room
	 * @return Member stub.
	 */
	@Override
	public IMember getMember() {
		return myMember;
	}

}
