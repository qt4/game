package qd2_qt4.client.message;

import common.IJoinRoomMsg;
import common.IMember;

/**
 * Known message, join room message.
 * @author qt4
 *
 */
public class JoinRoomMsg implements IJoinRoomMsg{
	/**
	 * Serial number.
	 */
	private static final long serialVersionUID = -4829993912818206261L;
	/**
	 * The member stub who joins the room.
	 */
	private IMember member;
	
	/**
	 * Constructor of the join message.
	 * @param member The member stub who joins the chat room.
	 */
	public JoinRoomMsg(IMember member){
		this.member = member;
	}
	
	/**
	 * Get the member stub who joins the room
	 * @return Member stub.
	 */
	@Override
	public IMember getMember() {
		return member;
	}

}
