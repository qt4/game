package qd2_qt4.client.message;

import common.IRejectMsg;

/**
 * Known message, reject message.
 * @author qt4
 *
 */
public class RejectMsg implements IRejectMsg{

	/**
	 * Serial number
	 */
	private static final long serialVersionUID = 8663915246521570353L;
	/**
	 * Reject reason.
	 */
	private String reason;
	
	/**
	 * Constructor of the reject message.
	 * @param reason Reject reason.
	 */
	public RejectMsg(String reason){
		this.reason = reason;
	}
	
	/**
	 * Get the reject reason.
	 * @return Reject reason.
	 */
	@Override
	public String getReasonText() {
		return reason;
	}

}
