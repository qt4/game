package qd2_qt4.client.view;

/**
 * Main view to model adapter.
 * @author qt4
 *
 */
public interface IClientModelAdapter {

	/**
	 * Connect to a server.
	 * @param text IP to connect.
	 * @return Return message.
	 */
	String connectTo(String text);

}
