package qd2_qt4.client.view;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JSplitPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import java.awt.GridLayout;

import qd2_qt4.client.view.IMiniModelAdapter;
import qd2_qt4.client.view.MiniGUI;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


/**
 * Main GUI of client MVC.
 * @author qt4
 *
 */
public class ClientGUI extends JFrame {

	/**
	 * Serial number
	 */
	private static final long serialVersionUID = 3703663265760688376L;
	private JPanel contentPane;
	private final JPanel panel = new JPanel();
	private final JLabel lblServerIp = new JLabel("Server IP:");
	private final JTextField txtIP = new JTextField();
	private final JButton btnConnect = new JButton("Connect");
	private final JSplitPane splitPane = new JSplitPane();
	private final JScrollPane scrollPane = new JScrollPane();
	private final JTextArea txtrDisplay = new JTextArea();
	private final JPanel panel_1 = new JPanel();
	
	/**
	 * Main view to model adapter.
	 */
	private IClientModelAdapter model;


	/**
	 * Create the frame.
	 * @param model A view to model adapter.
	 */
	public ClientGUI(IClientModelAdapter model) {
		txtIP.setToolTipText("Server IP address");
		txtIP.setText("localhost");
		txtIP.setColumns(10);
		this.model = model;
		initGUI();
	}
	/**
	 * Initialize the GUI.
	 */
	private void initGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		contentPane.add(panel, BorderLayout.NORTH);
		
		panel.add(lblServerIp);
		
		panel.add(txtIP);
		btnConnect.setToolTipText("Connect to the server IP");
		btnConnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				connect();
			}
		});
		
		panel.add(btnConnect);
		splitPane.setDividerSize(-1);
		splitPane.setEnabled(false);
		
		contentPane.add(splitPane, BorderLayout.CENTER);
		
		splitPane.setRightComponent(scrollPane);
		txtrDisplay.setEditable(false);
		txtrDisplay.setLineWrap(true);
		
		scrollPane.setViewportView(txtrDisplay);
		
		splitPane.setLeftComponent(panel_1);
		panel_1.setLayout(new GridLayout(1, 0, 0, 0));
	}
	/**
	 * Show the GUI
	 */
	public void start() {
		this.setVisible(true);
		
	}
	/**
	 * Show text messages
	 * @param s Text message to show.
	 */
	public void append(String s) {
		txtrDisplay.append(s);
		txtrDisplay.setCaretPosition(txtrDisplay.getText().length());
		
	}
	
	/**
	 * Message box to use.
	 * @param message Tip message.
	 * @param defaultValue Defaul value for the box.
	 * @return The input value.
	 */
	public String inputWindow(String message, String defaultValue) {
		String input = JOptionPane.showInputDialog(null, message, defaultValue);
		if (input!=null && input.equals("")) input = defaultValue;
		return input;
	}
	/**
	 * yes or no window for user 
	 * @param message message displayed on the window
	 * @param title title of the window
	 * @return user's choice
	 */
	public int yesNoWindow(String message, String title) {
		int reply = JOptionPane.showConfirmDialog(null, message, title, JOptionPane.YES_NO_OPTION);
		return reply;
	}

	/**
	 * connect to remote IPerson stub with given IP address
	 */
	private void connect() {
		append("Connecting...\n");
		append(model.connectTo(txtIP.getText()) + "\n");
	}

	/**
	 * Make a mini view.
	 * @param miniModelAdapter A mini view to model adapter.
	 * @return A mini view.
	 */
	public MiniGUI makeMiniGUI(IMiniModelAdapter miniModelAdapter) {
		MiniGUI miniGUI = new MiniGUI(miniModelAdapter);
		return miniGUI;
	}
}
