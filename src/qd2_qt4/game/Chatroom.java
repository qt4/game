package qd2_qt4.game;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import provided.datapacket.ADataPacket;
import common.IChatroom;
import common.IMember;

/**
 * Implementation of IChatroom.
 * @author qt4
 *
 */
public class Chatroom implements IChatroom{
	/**
	 * Serial number.
	 */
	private static final long serialVersionUID = -21324770723933284L;
	/**
	 * Team name.
	 */
	private String teamName;
	/**
	 * Team UUID.
	 */
	private UUID uuid = UUID.randomUUID();
	
	/**
	 * IMember list.
	 */
	private Set<IMember> memberList = new HashSet<IMember>();
	
	
	/**
	 * Constructor of Chatroom.
	 * @param name Team name.
	 */
	public Chatroom(String name){
		this.teamName = name;
	}
	
	/**
	 * Get team name
	 * @return Team name.
	 */
	@Override
	public String getName() {
		return teamName;
	}
	/**
	 * Get team UUID.
	 * @return Team UUID.
	 */
	@Override
	public UUID getUUID() {
		return uuid;
	}

	/**
	 * Get team members.
	 * @return Team members.
	 */
	@Override
	public Collection<IMember> getMembers() {
		return memberList;
	}

	/**
	 * Add members.
	 * @param userstub Member stubs to add.
	 */
	@Override
	public void addMembers(Collection<IMember> userstub) {
		memberList.addAll(userstub);
		
	}

	/**
	 * Add a member.
	 * @param member Member stub to add.
	 */
	@Override
	public void addMember(IMember member) {
		memberList.add(member);
	}

	/**
	 * Remove a member.
	 * @param userstub Member stub to remove.
	 */
	@Override
	public void removeMember(IMember userstub) {
		memberList.remove(userstub);
	}

	/**
	 * Broadcast messages to all members.
	 * @param msg Message to broadcast.
	 */
	@Override
	public Collection<ADataPacket> broadcastMsg(ADataPacket msg) {
		Collection<ADataPacket> dataPacketList = new HashSet<ADataPacket>();
		Set<IMember> tmpMemberList = new HashSet<IMember>(memberList);
		for(IMember member : tmpMemberList ){
			Thread thread = new Thread(new Runnable() {
				public void run() {
					try {
						dataPacketList.add(member.recvMessage(msg));
					} catch (RemoteException e) {
						e.printStackTrace();
						
					}
				}});
			thread.start();
			continue;
		}
		return dataPacketList;
	}

}
