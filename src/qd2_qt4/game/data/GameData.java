package qd2_qt4.game.data;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import common.IChatroom;

/**
 * Game data singleton
 * @author qt4
 *
 */
public class GameData {
	/**
	 * Game data singleton.
	 */
	public static GameData Singleton = new GameData();

	/**
	 * Alarm list.
	 */
	private Map<UUID, ArrayList<Double>> initAlarms = new Hashtable<UUID, ArrayList<Double>>();

	/**
	 * Key point list.
	 */
	private Map<UUID, ArrayList<Double>> keyPoints = new Hashtable<UUID, ArrayList<Double>>();

	/**
	 * Team scores.
	 */
	private Map<UUID, Integer> teamScores = new Hashtable<UUID, Integer>();
	/**
	 * Team key scores.
	 */
	private Map<UUID, Integer> teamKeys = new Hashtable<UUID, Integer>();

	/**
	 * Team names.
	 */
	private Map<UUID, String> teamNames = new Hashtable<UUID, String>();

	/**
	 * Team uuid map
	 */
	private Map<UUID, IChatroom> teamList = new Hashtable<UUID, IChatroom>();

	/**
	 * Constructor.
	 */
	private GameData(){
		initData();
	}

	/**
	 * Get the alarm list.
	 * @return Alarm list.
	 */
	public Map<UUID, ArrayList<Double>> getAlarms(){
		return initAlarms;
	}

	/**
	 * Get key point list.
	 * @return Key point list.
	 */
	public Map<UUID, ArrayList<Double>> getKeyPoints(){
		return keyPoints;
	}



	/**
	 * Set team scores.
	 * @param teamUUID UUID of the team to change score.
	 * @param score Score to add.
	 */
	public void setTeamScore(UUID teamUUID, int score){
		System.out.println("team UUID: "+teamUUID+" got "+ score);
		if (teamScores.containsKey(teamUUID)){
			int newScore = teamScores.get(teamUUID) + score;
			teamScores.put(teamUUID, newScore);
			System.out.println("team UUID: "+teamUUID+" new score: "+ newScore);
		} else {
			teamScores.put(teamUUID, score);
		}

	}


	/**
	 * Set team key points.
	 * @param teamUUID UUID of the team.
	 */
	public void setTeamKey(UUID teamUUID){
		System.out.println("team UUID: "+teamUUID+" got a key.");
		if (teamKeys.containsKey(teamUUID)){
			int newKey = teamKeys.get(teamUUID) + 1;
			teamKeys.put(teamUUID, newKey);
			System.out.println("team UUID: "+teamUUID+" new key: "+ newKey);
		} else {
			teamKeys.put(teamUUID, 0);
		}
	}

	/**
	 * Get team key point list.
	 * @return Key point list
	 */
	public Map<UUID, Integer> getTeamKeys(){
		return teamKeys;
	}

	/**
	 * Get team score list.
	 * @return Team score list.
	 */
	public Map<UUID, Integer> getTeamScores(){
		return teamScores;
	}


	/**
	 * Set the team name list.
	 * @param teamUUID Team UUID.
	 * @param name Name of the team.
	 */
	public void setTeamName(UUID teamUUID, String name){
		teamNames.put(teamUUID, name);
	}

	/**
	 * Get team name list.
	 * @return Team name list.
	 */
	public Map<UUID, String> getTeamName(){
		return teamNames;
	}


	/**
	 * Add a team to team list
	 * @param teamUUID Team UUID
	 * @param team Team to add.
	 */
	public void addTeam(UUID teamUUID, IChatroom team){
		teamList.put(teamUUID, team);
	}

	/**
	 * Get the team with a given UUID.
	 * @param teamUUID Team UUID
	 * @return Team of the given UUID.
	 */
	public IChatroom getTeam(UUID teamUUID){
		return teamList.get(teamUUID);
	}

	/**
	 * Get scores of all teams.
	 * @return Team scores to show.
	 */
	public ArrayList<String> getScores(){
		ArrayList<String> scores = new ArrayList<String>();
		Set<UUID> teams = teamNames.keySet();
		for (UUID team: teams){
			System.out.println("UUID in teams:" + team);
			scores.add("["+teamNames.get(team)+"]: "+teamScores.get(team));
			int keypoint = teamKeys.get(team);
			scores.add("           Key Point: "+ keypoint);
			if (keypoint>3) {
				scores.add("           Got Ships!");
			} else {
				scores.add("           No Ships");
			}
		}
		System.out.println("GameData scoreList:" + scores);
		return scores;
	}



	/**
	 * Initialize game data.
	 */
	public void initData(){
		//worldwide
		for (int i = 0; i<50; i++){
			ArrayList<Double> list = new ArrayList<Double>();
			list.add((double) randomInt(-90, 90));
			list.add((double) randomInt(-180, 180));
			list.add(10.0);

			initAlarms.put(UUID.randomUUID(), list);
		}

		//north america
		for (int i = 0; i<20; i++){
			ArrayList<Double> list = new ArrayList<Double>();
			list.add((double) randomInt(20, 60));
			list.add((double) randomInt(-70, -120));
			list.add(10.0);

			initAlarms.put(UUID.randomUUID(), list);
		}

		//south america
		for (int i = 0; i<20; i++){
			ArrayList<Double> list = new ArrayList<Double>();
			list.add((double) randomInt(-35, 0));
			list.add((double) randomInt(-75, -40));
			list.add(10.0);

			initAlarms.put(UUID.randomUUID(), list);
		}

		//africa
		for (int i = 0; i<20; i++){
			ArrayList<Double> list = new ArrayList<Double>();
			list.add((double) randomInt(-20, 30));
			list.add((double) randomInt(-5, 35));
			list.add(10.0);

			initAlarms.put(UUID.randomUUID(), list);
		}

		//asia
		for (int i = 0; i<20; i++){
			ArrayList<Double> list = new ArrayList<Double>();
			list.add((double) randomInt(20, 60));
			list.add((double) randomInt(60, 120));
			list.add(10.0);

			initAlarms.put(UUID.randomUUID(), list);
		}

		//europe
		for (int i = 0; i<20; i++){
			ArrayList<Double> list = new ArrayList<Double>();
			list.add((double) randomInt(45, 75));
			list.add((double) randomInt(45, 135));
			list.add(10.0);

			initAlarms.put(UUID.randomUUID(), list);
		}

		//_________________________ key point


		//north america
		for (int i = 0; i<5; i++){
			ArrayList<Double> list = new ArrayList<Double>();
			list.add((double) randomInt(20, 60));
			list.add((double) randomInt(-70, -120));
			list.add(30.0);

			UUID uuid = UUID.randomUUID();
			initAlarms.put(uuid, list);
			keyPoints.put(uuid, list);
		}

		//south america
		for (int i = 0; i<5; i++){
			ArrayList<Double> list = new ArrayList<Double>();
			list.add((double) randomInt(-25, 0));
			list.add((double) randomInt(-70, -45));
			list.add(30.0);

			UUID uuid = UUID.randomUUID();
			initAlarms.put(uuid, list);
			keyPoints.put(uuid, list);
		}

		//africa
		for (int i = 0; i<5; i++){
			ArrayList<Double> list = new ArrayList<Double>();
			list.add((double) randomInt(-20, 30));
			list.add((double) randomInt(-5, 35));
			list.add(30.0);

			UUID uuid = UUID.randomUUID();
			initAlarms.put(uuid, list);
			keyPoints.put(uuid, list);
		}

		//asia
		for (int i = 0; i<5; i++){
			ArrayList<Double> list = new ArrayList<Double>();
			list.add((double) randomInt(20, 60));
			list.add((double) randomInt(60, 120));
			list.add(30.0);

			UUID uuid = UUID.randomUUID();
			initAlarms.put(uuid, list);
			keyPoints.put(uuid, list);
		}

		//europe
		for (int i = 0; i<5; i++){
			ArrayList<Double> list = new ArrayList<Double>();
			list.add((double) randomInt(45, 65));
			list.add((double) randomInt(45, 135));
			list.add(30.0);

			UUID uuid = UUID.randomUUID();
			initAlarms.put(uuid, list);
			keyPoints.put(uuid, list);
		}
	}

	
	/**
	 * Reset data
	 */
	public void resetData(){
		initAlarms.clear();
		keyPoints.clear();
		teamScores.clear();
		teamNames.clear();
		teamKeys.clear();
		teamList.clear();
	}
	
	
	/**
	 * Random integer generator.
	 * @param min Minimum number
	 * @param max Maximum number
	 * @return A random number within the range of min and max.
	 */
	public int randomInt(int min, int max) {
		return (int)Math.floor((Math.random()*(1+max-min))+min);
	}
}
