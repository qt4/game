package qd2_qt4.game.view;

import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.globes.Earth;
import gov.nasa.worldwind.layers.IconLayer;
import gov.nasa.worldwind.layers.RenderableLayer;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JList;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;

import map.IRightClickAction;
import map.MapPanel;

import java.awt.Dimension;
import java.util.Vector;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;

import java.awt.Component;
import java.awt.Color;

import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextPane;

/**
 * Game view.
 * @author qt4
 *
 */
public class GameGUI extends JFrame {

	/**
	 * Serial number
	 */
	private static final long serialVersionUID = -7146496578210220681L;
	private JPanel contentPane;
	private final JPanel pnlMember = new JPanel();
	private final JPanel pnlChat = new JPanel();
	private final JScrollPane scrollPane = new JScrollPane();
	private final JTextArea txtrText = new JTextArea();	
	private IModelAdapter model;
	private final JPanel pnlDisplay = new JPanel();
	private final JScrollPane scrollPane_1 = new JScrollPane();

	private JList<String> lstMember;
	
	// for game
	/**
	 * Map panel
	 */
	private MapPanel _mapPanel = new MapPanel(Earth.class);

	private RenderableLayer sLayer = new RenderableLayer();
	private IconLayer iconLayer = new IconLayer();

	private final JPanel panel_1 = new JPanel();
	private final JList<String> lstTeamScore = new JList<String>();
	private final JTextPane txtpnTheGoal_1 = new JTextPane();
	
	
	
	/**
	 * Create the frame.
	 * @param model A view to model adapter.
	 */
	public GameGUI(IModelAdapter model) {
		lstTeamScore.setForeground(new Color(0, 51, 255));
		lstTeamScore.setPreferredSize(new Dimension(150, 600));
		lstTeamScore.setBorder(new TitledBorder(null, "Team Score", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		this.model = model;
		lstMember = new JList<String>(model.getMembers());
		lstMember.setForeground(new Color(0, 51, 255));
		initGUI();
		setLocationRelativeTo(null);

	}
	
	/**
	 * Initialize the game view.
	 */
	private void initGUI() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 1100, 700);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
			
		contentPane.add(pnlMember, BorderLayout.EAST);
		lstMember.setPreferredSize(new Dimension(150, 400));
		lstMember.setBorder(new TitledBorder(null, "Team Member", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GroupLayout gl_pnlMember = new GroupLayout(pnlMember);
		gl_pnlMember.setHorizontalGroup(
			gl_pnlMember.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_pnlMember.createSequentialGroup()
					.addContainerGap(12, Short.MAX_VALUE)
					.addGroup(gl_pnlMember.createParallelGroup(Alignment.LEADING)
						.addComponent(lstTeamScore, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lstMember, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap())
		);
		gl_pnlMember.setVerticalGroup(
			gl_pnlMember.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_pnlMember.createSequentialGroup()
					.addContainerGap()
					.addComponent(lstMember, GroupLayout.PREFERRED_SIZE, 164, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lstTeamScore, GroupLayout.PREFERRED_SIZE, 524, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		pnlMember.setLayout(gl_pnlMember);
		
		contentPane.add(pnlChat, BorderLayout.SOUTH);
		
		pnlChat.add(scrollPane);
		txtrText.setForeground(Color.RED);
		txtrText.setAlignmentX(Component.LEFT_ALIGNMENT);
		txtrText.setRows(5);
		txtrText.setLineWrap(true);
		txtrText.setColumns(45);
//		scrollPane.add(txtrText);
		scrollPane.setViewportView(txtrText);
		txtpnTheGoal_1.setEditable(false);
		txtpnTheGoal_1.setContentType("text/html");
		txtpnTheGoal_1.setText("1. The goal is to collect points. Team with the <b>HIGHEST SCORE</b> wins.<br>\r\n2. There are two kinds of points: <b>REGULAR(10pts)</b>, <b>KEY POINT(30pts)</b>.<br>&nbsp;&nbsp;&nbsp;&nbsp;With <b>4 key points</b>, the team will be offered <b>SHIPS</b> to travel by <b>SEA</b>.<br>\r\n3. <b>RIGHT CLICK</b> to move. <br>\r\n4. <font color=\"lime\"><b>SELF</b></font>; <font color=\"blue\"><b>TEAMMATE</b></font>; <font color=\"red\"> <b>COMPETITOR</b></font>.");
		
		pnlChat.add(txtpnTheGoal_1);
		
		contentPane.add(pnlDisplay, BorderLayout.WEST);
		
		pnlDisplay.add(scrollPane_1);
		
		
		
		
		
		// for game
		contentPane.add(_mapPanel, BorderLayout.CENTER);
//		_mapPanel.setPreferredSize(new java.awt.Dimension(600, 400));
		
		_mapPanel.add(panel_1, BorderLayout.NORTH);
		
		_mapPanel.addRightClickAction(new IRightClickAction(){

			@Override
			public void apply(Position p) {
				System.out.println(p);

				model.iconMove(p);
				model.myMove(p);

			}});

		_mapPanel.start();

		_mapPanel.addLayer(sLayer);
		_mapPanel.addLayer(iconLayer);

	
	}
	
	/**
	 * Start the game view.
	 */
	public void start(){
//		this.setVisible(true);
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
			    setVisible(true);
			}
		});
	}
	
	
	/**
	 * Update the user list.
	 */
	public void updateMemberList() {
		lstMember.updateUI();
		
	}
	
	/**
	 * Get the Box layer.
	 * @return A renderablelayer to put boxes.
	 */
	public RenderableLayer getBoxLayer(){
		return sLayer;
	}
	
	/**
	 * Get the icon layer.
	 * @return A iconlayer to put icons.
	 */
	public IconLayer getIconLayer(){
		return iconLayer;
	}

	/**
	 * Update team scores.
	 * @param scores Team scores.
	 */
	public void updateScores(Vector<String> scores) {
		System.out.println("GameGUI scoreList: "+ scores);
		lstTeamScore.setListData(scores);
	}
	
	/**
	 * Set the eye position
	 * @param pos Postion to view.
	 */
	public void setPosition(Position pos){
		_mapPanel.setPosition(pos, false);
	}

	/**
	 * Show text message.
	 * @param msg Message to show.
	 */
	public void showMsg(String msg) {
		txtrText.append(msg);
		txtrText.setCaretPosition(txtrText.getDocument().getLength());
	}
	
	/**
	 * Update the layer.
	 */
	public void update(){

		sLayer.firePropertyChange(AVKey.LAYER, null, null);
	}
}
