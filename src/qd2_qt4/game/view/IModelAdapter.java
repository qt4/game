package qd2_qt4.game.view;

import gov.nasa.worldwind.geom.Position;
import java.util.UUID;
import java.util.Vector;

/**
 * Game view to model adapter.
 * @author qt4
 */
public interface IModelAdapter {

	/**
	 * Get team members.
	 * @return Team members.
	 */
	Vector<String> getMembers();

	/**
	 * Send text message.
	 * @param msg Message to send.
	 */
	void sendMsg(String msg);

	/**
	 * Move player's box
	 * @param desPos Postion to move to.
	 */
	void myMove(Position desPos);

	/**
	 * Move a icon.
	 * @param p Postion to move to.
	 */
	void iconMove(Position p);

	/**
	 * Send player's positions.
	 * @param curPos Current position.
	 * @param desPos Destination position.
	 * @param onBoard If the player is on board.
	 */
	void sendPosition(Position curPos, Position desPos, boolean onBoard);

	/**
	 * Move other players.
	 * @param uuid UUID of the player.
	 * @param oriLat Original latitude.
	 * @param oriLon Original longitude.
	 * @param desLat Destination latitude.
	 * @param desLon Destination longitude.
	 * @param onBoard If the player is on board.
	 */
	void move(UUID uuid, double oriLat, double oriLon, double desLat,
			double desLon, boolean onBoard);

}
