package qd2_qt4.game.model;

import java.util.Vector;

import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.layers.IconLayer;
import gov.nasa.worldwind.layers.RenderableLayer;

/**
 * Game model to view adapter
 * @author qt4
 *
 */
public interface IViewAdapter {
	/**
	 * Get box layer.
	 * @return a renderablelayer to put boxes.
	 */
	RenderableLayer getBoxLayer();

	/**
	 * Get icon layer.
	 * @return an iconlayer to put icons.
	 */
	IconLayer getIconLayer();

	/**
	 * Update team scores.
	 * @param scoreList Score list of teams.
	 */
	void updateScores(Vector<String> scoreList);

	/**
	 * Set the centerposition of a box
	 * @param centerPosition Center position of a box.
	 */
	void setPosition(Position centerPosition);

	/**
	 * Show text message.
	 * @param msg Message to show.
	 */
	void showMsg(String msg);

	/**
	 * Set the team name on the window.
	 * @param name Team name.
	 */
	void setTitle(String name);

	/**
	 * Update the layers.
	 */
	void update();
}
