package qd2_qt4.game.model;

import gov.nasa.worldwind.Model;
import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.globes.ElevationModel;
import gov.nasa.worldwind.render.BasicShapeAttributes;
import gov.nasa.worldwind.render.Box;
import gov.nasa.worldwind.render.Ellipsoid;
import gov.nasa.worldwind.render.Material;
import gov.nasa.worldwind.render.PatternFactory;
import gov.nasa.worldwind.render.ShapeAttributes;
import gov.nasa.worldwind.render.UserFacingIcon;
import gov.nasa.worldwind.terrain.BathymetryFilterElevationModel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;
import java.util.UUID;
import java.util.Vector;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Sequencer;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

import provided.datapacket.DataPacket;
import qd2_qt4.server.message.CollectMsg;
import qd2_qt4.server.message.LocMsg;
import qd2_qt4.server.message.ShipLocMsg;
import qd2_qt4.server.message.TextMsg;
import common.IChatroom;
import common.IMember;
import common.ITextMsg;

/**
 * Model of the game.
 * @author qt4
 *
 */
public class GameModel {
	/**
	 * Model to view adapter.
	 */
	private IViewAdapter view;
	/**
	 * Team.
	 */
	private IChatroom team;
	/**
	 * UUID of the team.
	 */
	private UUID teamUUID;

	/**
	 * A team contains all players.
	 */
	private IChatroom allTeam;

	/**
	 * Team index.
	 */
	private int teamIdx;

	/**
	 * Local member stub.
	 */
	private IMember myMemberStub;

	/**
	 * Init alarm list.
	 */
	private Map<UUID, ArrayList<Double>> initAlarms = new Hashtable<UUID, ArrayList<Double>>();
	/**
	 * Pulsing icon list.
	 */
	private Map<UUID, PulsingIcon> alarmList = new Hashtable<UUID, PulsingIcon>();

	/**
	 * Image for pulsing icon.
	 */
	private BufferedImage circleYellow = createBitmap(PatternFactory.PATTERN_CIRCLE, Color.YELLOW);
	/**
	 * dest Icon UUID.
	 */
	private UUID desIconUUID = UUID.randomUUID();

	/**
	 * Player's box symbol
	 */
	private PlayerBox myBox;

	/**
	 * Player's UUID.
	 */
	private UUID myUUID;

	/**
	 * Player's ship.
	 */
	private Ship myShip;

	/**
	 * Destination icon.
	 */
	private PulsingIcon desIcon;
	/**
	 * All players' box list.
	 */
	private Map<UUID, PlayerBox> boxList = new Hashtable<UUID, PlayerBox>();
	/**
	 * All players' ship list.
	 */
	private Map<UUID, Ship> shipList = new Hashtable<UUID, Ship>();

	/**
	 * Init locations.
	 */
	private Map<Integer, double[]> initLoc = new Hashtable<Integer, double[]>();

	/**
	 * World wind model, used to get elevations. 
	 */
	private Model worldModel = (Model)WorldWind.createConfigurationComponent(AVKey.MODEL_CLASS_NAME);

	/**
	 * Constructor of the game model.
	 * @param view A model to view adapter.
	 */
	public GameModel(IViewAdapter view){
		this.view = view;

		//north america
		initLoc.put(1, new double[]{30, 50, -80, -116, 
				34, -78, 
				46.52, -124.1, 
				37.8, -122.6, 
				32.6, -117.1,
				23.23, -106.35,
				33.05, -79.50,
				39.63, -74.42,
				38.0, -122.9,
				43.8, -70.0});
		//south america
		initLoc.put(2, new double[]{-20, -4, -68, -50, 
				-7.4, -79.7,
				0.71, -80.04,
				-9.22, -78.66,
				-16.51, -72.99,
				-27.81, -71.14,
				-32.98, -71.61,
				-33.23, -52.98,
				-25.19, -48.15,
				-16.86, -39.07,
				-0.51, -47.76});
		//africa
		initLoc.put(3, new double[]{8, 29, -5, 30, 
				23.8, -16.06,
				12.20, -16.80,
				5.15, -4.11,
				-4.84, 11.90,
				-32.99, 27.61,
				-12.15, 40.44,
				-0.44, 42.47,
				9.38, 50.67});
		//asia
		initLoc.put(4, new double[]{27, 50, 70, 116, 
				20.62, 70.78,
				11.74, 75.48,
				17.11, 82.44,
				17.97, 94.48,
				22.89, 115.94,
				30.60, 122.48,
				34.73, 127.48,
				10.70, 107.45});


		//europe
		initLoc.put(5, new double[]{49, 62, 50, 134, 
				43.48, -8.78,
				48.32, -4.61,
				53.79, 6.84,
				62.82, 6.61,
				69.82, 18.56,
				75.48, 88.61,
				71.23, 157.13,
				61.32, 172.83});
	}

	/**
	 * Set the player's team
	 * @param team Team.
	 */
	public void setTeam(IChatroom team){
		this.team = team;	
		this.teamUUID = team.getUUID();
	}

	/**
	 * Set a team contains all players.
	 * @param allTeam A team contains all players.
	 */
	public void setAllTeam(IChatroom allTeam){
		this.allTeam = allTeam;
		System.out.println("allTeam size: "+ allTeam.getMembers().size());
	}

	/**
	 * Player's team index.
	 * @param idx Index of the team.
	 */
	public void setTeamIdx(int idx){
		this.teamIdx = idx;
	}


	/**
	 * Set the local member stub.
	 * @param myMemberStub Local member stub.
	 */
	public void setStub(IMember myMemberStub){
		this.myMemberStub = myMemberStub;
	}

	/**
	 * Get members from member list.
	 * @return Member list.
	 */
	public Vector<String> getMembers() {
		Vector<String> memberList = new Vector<String>();
		for (IMember member: team.getMembers()){
			try {
				memberList.add(member.getName());
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
		return memberList;
	}

	/**
	 * Start the model.
	 */
	public void start(){
		try {
			view.setTitle(team.getName());

			myUUID = myMemberStub.getUUID();
			// My player
			myBox = makePlayerBox(myUUID, myMemberStub.getName(), 
					Angle.fromDegrees(randomInt((int)initLoc.get(teamIdx)[0], (int)initLoc.get(teamIdx)[1])), 
					Angle.fromDegrees(randomInt((int)initLoc.get(teamIdx)[2], (int)initLoc.get(teamIdx)[3])), 
					Material.GREEN);
			view.getBoxLayer().addRenderable(myBox);

			view.setPosition(Position.fromDegrees(myBox.getCenterPosition().getLatitude().degrees, 
					myBox.getCenterPosition().getLongitude().degrees, 1e7));

			// My ship
			int myShipLoc = 2*randomInt(2, 5);
			myShip = makeShip(myUUID, myMemberStub.getName(), 
					Angle.fromDegrees(initLoc.get(teamIdx)[myShipLoc]), 
					Angle.fromDegrees(initLoc.get(teamIdx)[myShipLoc+1]), 
					Material.GREEN);
			view.getBoxLayer().addRenderable(myShip);

			// send out init location
			this.sendPosition(myBox.getCenterPosition(), myBox.getCenterPosition(), false);
			this.sendShipPosition(myShip.getCenterPosition(), myShip.getCenterPosition());

			BufferedImage circleRed = createBitmap(PatternFactory.PATTERN_CIRCLE, Color.GREEN);
			desIcon = new PulsingIcon(circleRed, Position.fromDegrees(27, -100), 20, desIconUUID);
			desIcon.setVisible(false);
			desIcon.setSize(new Dimension(10,10));
			desIcon.setAlwaysOnTop(true);
			view.getIconLayer().setRegionCulling(false);
			view.getIconLayer().addIcon(desIcon);

			// add alarms
			initAlarms();

			Map<IMember, Integer> temp = new Hashtable<IMember, Integer>();

			// add team mates
			for (IMember member: team.getMembers()){
				temp.put(member, 1);
				UUID uuid = member.getUUID();
				String name = member.getName();
				System.out.println("member UUID: "+uuid);
				if (!uuid.equals(myUUID)){
					PlayerBox box = makePlayerBox(uuid, name, 
							Angle.fromDegrees(randomInt((int)initLoc.get(teamIdx)[0], (int)initLoc.get(teamIdx)[1])), 
							Angle.fromDegrees(randomInt((int)initLoc.get(teamIdx)[2], (int)initLoc.get(teamIdx)[3])), 
							Material.BLUE);

					box.setVisible(false);
					boxList.put(uuid, box);
					view.getBoxLayer().addRenderable(box);


					//ships
					Ship ship = makeShip(uuid, name, 
							Angle.fromDegrees(initLoc.get(teamIdx)[4]), 
							Angle.fromDegrees(initLoc.get(teamIdx)[5]), 
							Material.BLUE);
					//					shipList.add(ship);
					shipList.put(uuid, ship);
					view.getBoxLayer().addRenderable(ship);
				}
			}

			//add other teams players
			for (IMember member: allTeam.getMembers()){

				if (!temp.containsKey(member)){
					UUID uuid = member.getUUID();
					String name = member.getName();
					PlayerBox box = makePlayerBox(uuid, name,
							Angle.fromDegrees(randomInt((int)initLoc.get(teamIdx)[0], (int)initLoc.get(teamIdx)[1])), 
							Angle.fromDegrees(randomInt((int)initLoc.get(teamIdx)[2], (int)initLoc.get(teamIdx)[3])), 
							Material.RED);

					box.setVisible(false);
					boxList.put(uuid, box);
					view.getBoxLayer().addRenderable(box);


					//ships
					Ship ship = makeShip(uuid, name, 
							Angle.fromDegrees(initLoc.get(teamIdx)[4]), 
							Angle.fromDegrees(initLoc.get(teamIdx)[5]), 
							Material.RED);
					shipList.put(uuid, ship);
					view.getBoxLayer().addRenderable(ship);
				}
			}


			// bathymetry remove
			// Get the current elevation model.
			ElevationModel currentElevationModel = worldModel.getGlobe().getElevationModel();
			// Wrap it with the no-bathymetry elevation model.
			BathymetryFilterElevationModel noDepthModel = new BathymetryFilterElevationModel(currentElevationModel);
			// Have the globe use the no-bathymetry elevation model.
			worldModel.getGlobe().setElevationModel(noDepthModel);


//			InputStream is;
//			try {
//				is = new URL("http://magicmusictutor.com/sites/default/files/Mario%20-%20Underwater%20Theme.mid").openStream();
//				Thread thread = new Thread(new Runnable() {
//					public void run() {
//						playTheme(is);
//					}});
//				thread.start(); 
//			
//			} catch (MalformedURLException e) {
//				e.printStackTrace();
//			} catch (IOException e) {
//				e.printStackTrace();
//			}

		} catch (RemoteException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Send text message.
	 * @param msg Message to send.
	 */
	public void sendMsg(String msg) {
		Thread thread = new Thread(new Runnable() {
			public void run() {
				team.broadcastMsg(new DataPacket<ITextMsg>(ITextMsg.class, myMemberStub, new TextMsg(msg)));			}
		});
		thread.start(); 
	}

	/**
	 * Send positions to others.
	 * @param curPos Current position.
	 * @param desPos Destination position.
	 * @param onBoard If the player is on ship.
	 */
	public void sendPosition(Position curPos, Position desPos, boolean onBoard) {
		Thread thread = new Thread( new Runnable() {
			public void run() {
					allTeam.broadcastMsg(new DataPacket<LocMsg>(LocMsg.class, myMemberStub, 
						new LocMsg(curPos.getLatitude().degrees, curPos.getLongitude().degrees, 
								desPos.getLatitude().degrees, desPos.getLongitude().degrees, onBoard)));
			}
		});
		thread.start(); 
	}

	/**
	 * Send ship positions to others.
	 * @param curPos Ship's current position.
	 * @param desPos Ship's destination position.
	 */
	public void sendShipPosition(Position curPos, Position desPos) {
		Thread thread = new Thread( new Runnable() {
			public void run() {
				allTeam.broadcastMsg(new DataPacket<ShipLocMsg>(ShipLocMsg.class, myMemberStub, 
						new ShipLocMsg(curPos.getLatitude().degrees, curPos.getLongitude().degrees, 
								desPos.getLatitude().degrees, desPos.getLongitude().degrees)));
			}
		});
		thread.start(); 
	}

	/**
	 * Move players's boxes according to given params.
	 * @param uuid UUID of the box.
	 * @param oriLat Original latitude.
	 * @param oriLon Original longitude.
	 * @param desLat Destination latitude.
	 * @param desLon Destination longitude.
	 * @param onBoard If the player is on board.
	 */
	public void movePlayer(UUID uuid, double oriLat, double oriLon, double desLat, double desLon, boolean onBoard) {
		System.out.println("targeted UUID: "+uuid);
		System.out.println("box list size: "+boxList.size());
		if (uuid.equals(myUUID)) return;

		PlayerBox box = boxList.get(uuid);
		Ship ship = shipList.get(uuid);
		if (box!=null && ship!=null){
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					Thread.currentThread().setPriority(Thread.MAX_PRIORITY);

					if (!onBoard){
						box.moveTo(Position.fromDegrees(oriLat, oriLon));
						box.move(oriLat, oriLon, desLat, desLon);
						box.setVisible(true);
					} else {
						box.setVisible(false);
						System.out.println("On board");
						ship.move(oriLat, oriLon, desLat, desLon);
					}
					
				}
			});
		}



	}


	/**
	 * Move other's ship according to given params.
	 * @param uuid UUID of the ship to move.
	 * @param oriLat Ship's original latitude.
	 * @param oriLon Ship's original longitude.
	 * @param desLat Ship's destination latitude.
	 * @param desLon Ship's destination longitude.
	 */
	public void moveShip(UUID uuid, double oriLat, double oriLon, double desLat, double desLon) {
		System.out.println("targeted UUID: "+uuid);
		System.out.println("box list size: "+boxList.size());

		Ship ship = shipList.get(uuid);
		if (ship!=null){
			ship.move(oriLat, oriLon, desLat, desLon);
		}

		view.update();
	}




	/**
	 * Move player's box to a position.
	 * @param desPos Destination position.
	 */
	public void myMove(Position desPos){
		if (myBox.isVisible()){
			Position cur = myBox.getCenterPosition();
			myBox.move(cur.getLatitude().degrees, cur.getLongitude().degrees, 
					desPos.getLatitude().degrees, desPos.getLongitude().degrees);
			this.sendPosition(cur, desPos, false);
		} else {
			Position cur = myShip.getCenterPosition();
			myShip.move(cur.getLatitude().degrees, cur.getLongitude().degrees, 
					desPos.getLatitude().degrees, desPos.getLongitude().degrees);
			this.sendPosition(cur, desPos, true);
		}

	}

	/**
	 * Make a player box.
	 * @param uuid UUID of a player.
	 * @param playerName Name of a player.
	 * @param lat Latitude of the box.
	 * @param lon Longitude of the box.
	 * @param color Color of the box.
	 * @return A PlayerBox object.
	 */
	public PlayerBox makePlayerBox(UUID uuid, String playerName, Angle lat, Angle lon, Material color) {

		ShapeAttributes attrs = new BasicShapeAttributes();
		attrs.setDrawOutline(false);
		attrs.setInteriorMaterial(color);
		attrs.setEnableLighting(true);		


		PlayerBox box4 = new PlayerBox(uuid, Position.fromDegrees(lat.degrees, lon.degrees, 50000), 50000, 50000, 50000, null);
		box4.setAltitudeMode(WorldWind.RELATIVE_TO_GROUND);
		box4.setAttributes(attrs);
		box4.setVisible(true);
		box4.setValue(AVKey.DISPLAY_NAME, playerName);
		return box4;
	}


	
	/**
	 * Random integer generator.
	 * @param min Minimum
	 * @param max Maximum
	 * @return A random integer in the range between min and max.
	 */
	public int randomInt(int min, int max) {
		return (int)Math.floor((Math.random()*(1+max-min))+min);
	}



	/**
	 * A PlayerBox represents a player.
	 * @author qt4
	 *
	 */
	private class PlayerBox extends Box{
		/**
		 * Location interpolated point.
		 */
		private double n = 0;
		/**
		 * Original position.
		 */
		private Position oriPos;
		/**
		 * Current position.
		 */
		private Position curPos;
		/**
		 * Destination position.
		 */
		private Position desPos;
		/**
		 * Angle distance between two positions.
		 */
		private Angle ang;
		/**
		 * Previous position.
		 */
		private Position prevPos;
		/**
		 * Timer for moving.
		 */
		private Timer timer;
		/**
		 * Constructor of PlayerBox.
		 * @param uuid UUID of the player.
		 * @param centerPosition Center position of the box.
		 * @param northSouthRadius North to south length of the box.
		 * @param verticalRadius Height of the box.
		 * @param eastWestRadius East to west length of the box.
		 * @param limit Moving limits.
		 */
		private PlayerBox(UUID uuid, Position centerPosition,
				double northSouthRadius,
				double verticalRadius,
				double eastWestRadius, double[] limit){
			super(centerPosition, northSouthRadius, verticalRadius, eastWestRadius);
			this.oriPos = this.getCenterPosition();
			this.curPos = this.getCenterPosition();
			this.desPos = this.getCenterPosition();
			this.ang = Position.greatCircleDistance(oriPos, desPos);
//			this.uuid = uuid;
//			if (limit!=null)
//				this.limit = limit;

			if (timer==null) {
				timer = new Timer(200, new ActionListener() {
					public void actionPerformed(ActionEvent e) {		
						//calculate speed: 1.0/5 Nautical mile/200 msec
						double speed = 1.0/2/ang.degrees;
						n=n+speed;
						curPos = Position.interpolateGreatCircle(n, oriPos, desPos);
						prevPos = Position.interpolateGreatCircle(n-speed, oriPos, desPos);
						PlayerBox.this.setHeading(LatLon.greatCircleAzimuth(oriPos, desPos));
						if (worldModel.getGlobe().getElevationModel().getElevation(curPos.getLatitude(), curPos.getLongitude())>5) {
							PlayerBox.this.moveTo(curPos);

						} else {
							n=2;
							desPos = prevPos;
							curPos = prevPos;
						}
						view.update();
	
					}		 
				});
				timer.start();
			}
		}

		/**
		 * Move the PlayerBox
		 * @param oriLat Original latitude.
		 * @param oriLon Original longitude.
		 * @param desLat Destination latitude.
		 * @param desLon Destination longitude.
		 */
		protected void move(double oriLat, double oriLon, double desLat, double desLon){
			n = 0;
			oriPos = Position.fromDegrees(oriLat, oriLon, 50000);
			desPos = Position.fromDegrees(desLat, desLon, 50000);
			ang = Position.greatCircleDistance(oriPos, desPos);
		}

		/**
		 * Stop the timer.
		 */
		protected void stop(){
			timer.stop();
		}

	}


	/**
	 * Create a blurred pattern bitmap for PulsingIcon
	 * @param pattern Pattern of the image.
	 * @param color Color of the image.
	 * @return A buffered image.
	 */
	private BufferedImage createBitmap(String pattern, Color color){
		// Create bitmap with pattern
		BufferedImage image = PatternFactory.createPattern(pattern, new Dimension(128, 128), 0.7f,
				color, new Color(color.getRed(), color.getGreen(), color.getBlue(), 0));
		// Blur a lot to get a fuzzy edge
		image = PatternFactory.blur(image, 13);
		image = PatternFactory.blur(image, 13);
		image = PatternFactory.blur(image, 13);
		image = PatternFactory.blur(image, 13);
		return image;
	}

	/**
	 * Pulsing icon represents alarms.
	 * @author qt4
	 *
	 */
	private class PulsingIcon extends UserFacingIcon{
		/**
		 * Icon path
		 */
		protected final Object bgIconPath;
		/**
		 * Scale factor of the icon image.
		 */
		protected int scaleIndex = 0;
		/**
		 * Scale steps.
		 */
		protected double[] scales = new double[] {1.25, 1.5, 1.75, 2, 2.25, 2.5, 2.75, 3, 3.25, 3.5, 3.25, 3,
				2.75, 2.5, 2.25, 2, 1.75, 1.5};
		/**
		 * Timer for pulsing.
		 */
		protected Timer timer;

		/**
		 * Constructor of PulsingIcon.
		 * @param imageSource Source of image.
		 * @param pos Position of the icon.
		 * @param frequency Pulsing frequency.
		 * @param uuid UUID of the icon.
		 */
		private PulsingIcon(Object imageSource, Position pos, int frequency, UUID uuid){

			super(imageSource, pos);
			this.bgIconPath = imageSource;
	
			if (timer == null) {
				timer = new Timer(frequency, new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						PulsingIcon.this.setBackgroundScale(scales[++scaleIndex % scales.length]);
						view.update();
							if (Position.greatCircleDistance(myBox.getCenterPosition(), PulsingIcon.this.getPosition()).degrees<0.01 ||
								Position.greatCircleDistance(myShip.getCenterPosition(), PulsingIcon.this.getPosition()).degrees<0.05){
							timer.stop();
							PulsingIcon.this.setVisible(false);
							sendCollectMsg(uuid);
	
						}
					}
				});
			}
			this.setBackgroundImage(bgIconPath);
			scaleIndex = 0;

			timer.start();
		}

		/**
		 * Star the pulsing timer.
		 */
		private void starTimer(){
			timer.start();
		}
	}

	/**
	 * Move destination icon to a given position.
	 * @param p Position to move the icon to.
	 */
	public void iconMove(Position p) {
		desIcon.starTimer();
		desIcon.moveTo(p);
		desIcon.setVisible(true);
	}

	/**
	 * Add icons to given location.
	 * @param key UUID of icons.
	 * @param pos Positions of icons.
	 */
	public void addAlarm(UUID key, Position pos){
		PulsingIcon icon = new PulsingIcon(circleYellow, pos, 100, key);
		alarmList.put(key, icon);

		icon.setSize(new Dimension(20, 20));
		icon.setVisible(true);
		view.getIconLayer().addIcon(icon);
	}

	/**
	 * Remove a icon with given UUID.
	 * @param key UUID of a icon.
	 */
	public void removeAlarm(UUID key){
		if (!key.equals(desIconUUID)) {
			System.out.println("alarm list: "+alarmList.size());
			System.out.println("alarm: "+alarmList.get(key));

			if (alarmList.containsKey(key)){
				alarmList.get(key).setVisible(false);
				view.getIconLayer().removeIcon(alarmList.get(key));
				alarmList.remove(key);
				System.out.println("alarm list: "+alarmList.size());
			}
		}
	}

	
	/**
	 * Init all the alarms.
	 */
	public void initAlarms(){
		for(UUID key: initAlarms.keySet()){
			addAlarm(key, Position.fromDegrees(initAlarms.get(key).get(0), initAlarms.get(key).get(1)));
		}
	}

	/**
	 * Set the alarm list.
	 * @param map Given list to set.
	 */
	public void setInitAlarms(Map<UUID, ArrayList<Double>> map){
		initAlarms = map;
	}


	/**
	 * Send collect message to others.
	 * @param uuid UUID of collected icons.
	 */
	public void sendCollectMsg(UUID uuid) {
		if (!uuid.equals(desIconUUID)) {
			Thread thread = new Thread( new Runnable() {
				public void run() {
					allTeam.broadcastMsg(new DataPacket<CollectMsg>(CollectMsg.class, myMemberStub,
							new CollectMsg(uuid, teamUUID)));
				}
			});
			thread.start(); 
		}
	}





	/**
	 * A ship object for player.
	 * @author qt4
	 *
	 */
	private class Ship extends Ellipsoid{
		/**
		 * Location interpolated point.
		 */
		private double n = 0;
		/**
		 * Ship's original position.
		 */
		private Position oriPos;
		/**
		 * Ship's current position.
		 */
		private Position curPos;
		/**
		 * Ship's destination position.
		 */
		private Position desPos;
		/**
		 * The angle distance between ori and des positions.
		 */
		private Angle ang;
		/**
		 * Previous position.
		 */
		private Position prevPos;
		/**
		 * Timer for moving.
		 */
		private Timer timer;
		/**
		 * Constructor for ship.
		 * @param uuid UUID of the ship.
		 * @param centerPosition Center position of the ship.
		 * @param northSouthRadius North to south length of the ship.
		 * @param verticalRadius Height of the ship.
		 * @param eastWestRadius East to west length of the ship.
		 */
		private Ship(UUID uuid, Position centerPosition,
				double northSouthRadius,
				double verticalRadius,
				double eastWestRadius){
			super(centerPosition, northSouthRadius, verticalRadius, eastWestRadius);
			this.oriPos = this.getCenterPosition();
			this.curPos = this.getCenterPosition();
			this.desPos = this.getCenterPosition();
			this.ang = Position.greatCircleDistance(oriPos, desPos);
//			this.uuid = uuid;

			if (timer==null){
				timer = new Timer(100, new ActionListener() {
					public void actionPerformed(ActionEvent e) {		
						//calculate speed: 1.0/5 Nautical mile/200 msec
						double speed = 1.0/ang.degrees;
						n=n+speed;
						curPos = Position.interpolateGreatCircle(n, oriPos, desPos);
						prevPos = Position.interpolateGreatCircle(n-speed, oriPos, desPos);
						Ship.this.setHeading(LatLon.greatCircleAzimuth(oriPos, desPos));
						if (worldModel.getGlobe().getElevationModel().getElevation(curPos.getLatitude(), curPos.getLongitude())<3) {
							Ship.this.moveTo(curPos);

						} else {			
							if (!myBox.isVisible() && Position.greatCircleDistance(desPos, Ship.this.getCenterPosition()).degrees<6 &&
									worldModel.getGlobe().getElevationModel().getElevation(desPos.getLatitude(), desPos.getLongitude())>10){	
								myBox.moveTo(desPos);
								myBox.move(desPos.getLatitude().degrees, desPos.getLongitude().degrees, 
										desPos.getLatitude().degrees, desPos.getLongitude().degrees);

								myBox.setVisible(true);
							} else {

								n=2;
								desPos = prevPos;
								curPos = prevPos;
							}
						}
	
						if (Position.greatCircleDistance(myBox.getCenterPosition(), Ship.this.getCenterPosition()).degrees<2){
							myBox.setVisible(false);
						}
						view.update();
					}		 
				});
			}
		}

		/**
		 * Move the ship
		 * @param oriLat Ship's original latitude.
		 * @param oriLon Ship's original longitude.
		 * @param desLat Ship's destination latitude.
		 * @param desLon Ship's destination longitude.
		 */
		protected void move(double oriLat, double oriLon, double desLat, double desLon){
			n = 0;
			oriPos = Position.fromDegrees(oriLat, oriLon, 50000);
			desPos = Position.fromDegrees(desLat, desLon, 50000);
			ang = Position.greatCircleDistance(oriPos, desPos);
		}

		/**
		 * Start the timer.
		 */
		protected void start(){
			timer.start();
		}

		/**
		 * Stop the timer.
		 */
		protected void stop(){
			timer.stop();
		}
	}


	/**
	 * Make a ship with given params.
	 * @param uuid UUID of the ship.
	 * @param playerName Name of the player.
	 * @param lat Latitude of the ship.
	 * @param lon Longitude of the ship.
	 * @param color Color of the ship.
	 * @return A ship object.
	 */
	public Ship makeShip(UUID uuid, String playerName, Angle lat, Angle lon, Material color) {

		ShapeAttributes attrs = new BasicShapeAttributes();
		attrs.setDrawOutline(false);
		attrs.setInteriorMaterial(color);
		attrs.setEnableLighting(true);		


		Ship ship = new Ship(uuid, Position.fromDegrees(lat.degrees, lon.degrees, 50000), 3e5, 6e4, 1e5);
		ship.setAltitudeMode(WorldWind.RELATIVE_TO_GROUND);
		ship.setAttributes(attrs);
		ship.setVisible(false);
		ship.setValue(AVKey.DISPLAY_NAME, playerName);
		return ship;
	}


	/**
	 * Show given ships.
	 * @param uuidList UUIDs of the ships to show.
	 */
	public void showShip(ArrayList<UUID> uuidList) {
		System.out.println("ship uuid list: "+ uuidList);
		for (UUID uuid: uuidList) {
			if (uuid.equals(myUUID)) {
				myShip.start();
				myShip.setVisible(true);
			} else {
				shipList.get(uuid).start();
				shipList.get(uuid).setVisible(true);
			}
		}

	}


	/**
	 * Update team scores.
	 * @param scores Team scores.
	 */
	public void updateScores(ArrayList<String> scores) {
		System.out.println("scores received: "+scores);
		Vector<String> scoreList = new Vector<String>();
		for (String score: scores){
			try {
				scoreList.add(score);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		System.out.println("scoreList generated: "+scoreList);
		view.updateScores(scoreList);
	}

	/**
	 * Show text messages.
	 * @param msg Text message to show.
	 */
	public void showMsg(String msg){
		view.showMsg(msg);
	}

	/**
	 * Stop the game
	 * @param msg Winner message.
	 */
	public void stopGame(String msg){
		myBox.stop();
		myShip.stop();
		JOptionPane.showMessageDialog(null, msg, "The Game is Over.", JOptionPane.INFORMATION_MESSAGE);
	}

	/**
	 * Play game music.
	 * @param is Midi stream
	 */
	public void playTheme(InputStream is){
		try {
			Sequencer sequencer = MidiSystem.getSequencer();
			sequencer.open();
			sequencer.setSequence(is);
			int loop = 2;
			while(loop>0){
				sequencer.start();
				loop = 2;
				sequencer.setLoopCount(loop);
			}
		} catch (MidiUnavailableException | IOException | InvalidMidiDataException e) {
			e.printStackTrace();
		}

	}
}
