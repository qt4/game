package qd2_qt4.game.controller;

import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.layers.IconLayer;
import gov.nasa.worldwind.layers.RenderableLayer;

import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;
import java.util.Vector;

import common.IChatroom;
import common.IMember;
import qd2_qt4.game.model.GameModel;
import qd2_qt4.game.model.IViewAdapter;
import qd2_qt4.game.view.GameGUI;
import qd2_qt4.game.view.IModelAdapter;

/**
 * Controller of the game.
 * @author qt4
 * 
 */
public class GameController {
	/**
	 * Model of the MVC.
	 */
	private GameModel model;
	/**
	 * View of the MVC
	 */
	private GameGUI view;
	
	
	/**
	 * Constructor of the controller.
	 * @param team The team.
	 */
	public GameController(IChatroom team) {
		model = new GameModel(new IViewAdapter(){

			@Override
			public RenderableLayer getBoxLayer() {
				return view.getBoxLayer();
			}

			@Override
			public IconLayer getIconLayer() {
				return view.getIconLayer();
			}

			@Override
			public void updateScores(Vector<String> scoreList) {
				view.updateScores(scoreList);
				
			}

			@Override
			public void setPosition(Position pos) {
				view.setPosition(pos);
				
			}

			@Override
			public void showMsg(String msg) {
				view.showMsg(msg);
				
			}

			@Override
			public void setTitle(String name) {
				view.setTitle(name);
				
			}

			@Override
			public void update() {
				view.update();
				
			}
			
			
		});
		
		model.setTeam(team);

		view = new GameGUI(new IModelAdapter(){

			@Override
			public Vector<String> getMembers() {
				return model.getMembers();
			}

			@Override
			public void sendMsg(String msg) {
				model.sendMsg(msg);
				
			}

			@Override
			public void sendPosition(Position curPos, Position desPos, boolean onBoard) {
				model.sendPosition(curPos, desPos, onBoard);
			}


			@Override
			public void move(UUID uuid, double oriLat, double oriLon,
					double desLat, double desLon, boolean onBoard) {
				model.movePlayer(uuid, oriLat, oriLon, desLat, desLon, onBoard);
				
			}

			@Override
			public void myMove(Position desPos) {
				model.myMove(desPos);
				
			}

			@Override
			public void iconMove(Position p) {
				model.iconMove(p);
				
			}
			
		});
	}

	public void setMemberStub(IMember memberStub){
		model.setStub(memberStub);
	}
	/**
	 * Start main model and view.
	 */
	public void start() {	
		view.start();
		model.start();
	}
	
	
	public GameModel getGame(){
		return model;
	}


	public void setInitAlarms(Map<UUID, ArrayList<Double>> initAlarms) {
		model.setInitAlarms(initAlarms);
	}

	public void setAllTeam(IChatroom allTeam) {
		model.setAllTeam(allTeam);
		
	}
	
	public void setTeamIdx(int idx){
		model.setTeamIdx(idx);
	}
}
